import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import './index.scss';
import App from './App';
// import 'tachyons';
import 'typeface-roboto';
import * as serviceWorker from './serviceWorker';
import { SnackbarProvider } from 'notistack';
import { Provider } from 'mobx-react';
import 'bootstrap/dist/css/bootstrap.min.css';

ReactDOM.render(
	<Provider>
		<BrowserRouter>
			<SnackbarProvider maxSnack={3}>
				<App />
			</SnackbarProvider>
		</BrowserRouter>
	</Provider>,
	document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
