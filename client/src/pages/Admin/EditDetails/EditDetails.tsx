import React, {Component} from "react";
import {Fab, Typography, CircularProgress} from "@material-ui/core/";
import {withSnackbar, WithSnackbarProps} from "notistack";
import SaveIcon from "@material-ui/icons/Save";
import NavigationBar from "../../../components/Admin/NavigationBar/NavigationBar";
import "../../../style.scss";
import {RouteComponentProps} from "react-router";

interface IManageUserProps
  extends RouteComponentProps<any>,
    WithSnackbarProps {}

interface IManageUserStates {
  user: {
    fullname: string;
    username: string;
    password: string;
  };
  title: string;
  isLoading: boolean;
}

class ManageUser extends Component<IManageUserProps, IManageUserStates> {
  constructor(props: IManageUserProps) {
    super(props);
    this.state = {
      user: {
        fullname: "",
        username: "",
        password: ""
      },
      title: "Edit Personal Details",
      isLoading: true
    };
  }

  componentDidMount() {
    // Requesting all information of users
    // api.getUser(auth.getUser().id).then(res => this.setState({ user: res.data[0], title: 'Manage User', isLoading: false, isEditEmployee: true }))
  }

  updateInputValue = (event: any) => {
    // Update user field in accordance with the id on the input
    this.setState({
      user: {...this.state.user, [event.target.id]: event.target.value}
    });
  };

  saveChangesHandler = () => {
    this.setState({isLoading: true});

    // Requesting user to update with the information changed
    // api.updateUser(this.state.user).then(res => {
    //   this.props.enqueueSnackbar('Changes saved.', { variant: 'success' })
    // }).catch(res => {
    //   this.props.enqueueSnackbar('Cannot save changes.', { variant: 'error' })
    // }).finally(() => {
    //   this.setState({ isLoading: false });
    // })
  };

  render() {
    const {user, title, isLoading} = this.state;
    return (
      <div>
        <NavigationBar title={title} actionComponents={null} />
        <div className="pa3">
          {isLoading === true ? (
            <div className="flex justify-center items-center pa5">
              <CircularProgress />
            </div>
          ) : (
            <div>
              <Typography paragraph variant="h6" color="primary">
                Personal Details
              </Typography>
              <Typography paragraph variant="body1">
                Full Name
              </Typography>
              <input
                className="w-100 mb3"
                id="fullname"
                type="text"
                placeholder="Full Name"
                value={user.fullname}
                onChange={this.updateInputValue}
              />
              <Typography paragraph variant="body1">
                Username: {user.username}
              </Typography>
              <Typography paragraph variant="body1">
                Password
              </Typography>
              <input
                className="w-100 mb3"
                id="password"
                type="password"
                placeholder="Password"
                value={user.password}
                onChange={this.updateInputValue}
              />
            </div>
          )}
        </div>
        <div className="fab">
          <Fab
            color="primary"
            aria-label="add"
            onClick={this.saveChangesHandler}
          >
            <SaveIcon />
          </Fab>
        </div>
      </div>
    );
  }
}

export default withSnackbar(ManageUser);
