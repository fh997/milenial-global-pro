import React, {Component} from "react";
import Grid from "@material-ui/core/Grid";
import Jumbotron from "../../components/Jumbotron/Jumbotron";
import ContactBar from "../../components/ContactBar/ContactBar";
import Footer from "../../components/Footer/Footer";
import ProfileCard from "../../components/ProfileCard/ProfileCard";
import Logo from "../../assets/images/logo.png";
import "./About.scss";
import {RouteComponentProps} from "react-router";
import {GetEmployeeOutput} from "../../services/employee/dto/getEmployeeOutput";
import employeeService from "../../services/employee/employeeService";

interface IAboutProps extends RouteComponentProps<any> {}

interface IAboutStates {
  employees: GetEmployeeOutput[];
}

class About extends Component<IAboutProps, IAboutStates> {
  constructor(props: IAboutProps) {
    super(props);
    this.state = {
      employees: []
    };
  }

  componentDidMount() {
    document.title = "About Us | Milenial Global Pro";
    // Requesting information of all employees from the api
    employeeService.getAll().then(res => {
      this.setState({employees: res.data.data});
    });
  }

  render() {
    const {employees} = this.state;
    return (
      <div className="about-page">
        <Jumbotron title="About Us" />
        <div className="container py-4">
          <div className="company-info">
            <h4>Misi Kami</h4>
            <ul>
              <li>Menjadi pemimpin di pasar properti</li>
              <li>Membantu setiap keluarga untuk memiliki tempat tinggal</li>
              <li>
                Membantu pemerintah dalam hal perumahan untuk masyarakat yang
                membutuhkan tempat tinggal yang layak
              </li>
            </ul>
          </div>
        </div>

        <div className="container py-4">
          <div className="company-info">
            <Grid container spacing={5}>
              <Grid item xs={12} sm={6} md={6}>
                <h4>Perusahaan Kami</h4>
                <p>
                  Milenial Global Pro adalah sebuah perusahaan yang kami dirikan
                  secara resmi tanggal 09 September 2019 untuk menjadi sarana
                  yang lebih pasti dalam menjalankan usaha kami yang telah
                  berjalan sebelum berdirinya perusahaan ini dengan tujuan
                  memudahkan klien kami untuk mendapat kepastian ketika memilih
                  properti.
                </p>
                <br />
                <p>
                  Perusahaan kami saat ini dipimpin oleh IBU MELISANI sebagai
                  pemilik perusahaan, dengan dibantu oleh IBU FANI CHANDRA
                  sebagai associate director, dan juga beberapa orang staf
                  lainnya.
                </p>
                <br />
                <p>
                  Sebagai sebuah perusahaan yang sudah memiliki izin resmi
                  tentunya akan memudahkan kami dan klien kami untuk mengurus
                  semua surat yang berhubungan dengan bisnis kami. Kami juga
                  dibantu oleh tenaga pemasaran yang kompeten untuk menunjang
                  perkembangan perusahaan kami, dengan harapan kami bisa
                  membantu sebanyak mungkin orang untuk memenuhi kebutuhannya di
                  bidang properti. Tak lupa kami ucapkan terima kasih kepada
                  mereka yang sudah membantu kami selama ini dalam mengakomodasi
                  kami sampai terbentuknya usaha ini, dan tak lupa kami ucapkan
                  terima kasih.
                </p>
              </Grid>
              <Grid item xs={12} sm={6} md={6} className="tc">
                <img src={Logo} alt="Milenial Global Pro" />
              </Grid>
            </Grid>
          </div>
        </div>

        <div className="company-info py-4 secondary">
          <div className="container">
            <h4 className="py-3">Tim Kami</h4>
            <Grid container spacing={2}>
              {employees.map((entry, i) => {
                return (
                  <Grid item xs={12} sm={4} key={i}>
                    <ProfileCard data={entry} onClick={null} />
                  </Grid>
                );
              })}
            </Grid>
          </div>
        </div>

        <ContactBar />
        <Footer />
      </div>
    );
  }
}

export default About;
