import React from "react";
import {Grid} from "@material-ui/core/";
import BuyLogo from "../../assets/images/workflow_1.png";
import SellLogo from "../../assets/images/workflow_2.png";
import RentLogo from "../../assets/images/workflow_3.png";
import "./ServiceBar.scss";

const ServiceBar = () => {
  return (
    <section className="servicebar container">
      <Grid container>
        <Grid item xs={12} sm={12} md={4} className="service-type">
          <img src={BuyLogo} alt="Buy" />
          <h4>Buy</h4>
          <p>Tertarik untuk membeli properti? Kami bisa membantu anda</p>
        </Grid>
        <Grid item xs={12} sm={12} md={4} className="service-type">
          <img src={SellLogo} alt="Sell" />
          <h4>Sell</h4>
          <p>Memikirkan untuk menjual rumah? Kami bisa membantu anda</p>
        </Grid>
        <Grid item xs={12} sm={12} md={4} className="service-type">
          <img src={RentLogo} alt="Rent" />
          <h4>Rent</h4>
          <p>
            Tertarik untuk menyewakan properti anda? Atau tertarik untuk menyewa
            properti? Kami bisa membantu anda
          </p>
        </Grid>
      </Grid>
    </section>
  );
};

export default ServiceBar;
