export interface GetAllEmployeeInput {
    filter: string | null;
    skipCount: number | null;
    maxResultCount: number | null;
}