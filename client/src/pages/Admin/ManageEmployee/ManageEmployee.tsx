import React, {Component} from "react";
import {Grid, Fab, Typography, IconButton, Button, Backdrop, CircularProgress} from "@material-ui/core/";
import SaveIcon from "@material-ui/icons/Save";
import DeleteIcon from "@material-ui/icons/Delete";
import NavigationBar from "../../../components/Admin/NavigationBar/NavigationBar";
import employeeService from "../../../services/employee/employeeService";
import {RouteComponentProps} from "react-router";
import {CreateOrEditEmployee} from "../../../services/employee/dto/createOrEditEmployee";
import {withSnackbar, WithSnackbarProps} from "notistack";
import "../../../style.scss";

interface IManageEmployeeProps
  extends RouteComponentProps<any>,
    WithSnackbarProps {}

interface IManageEmployeeStates {
  employee: CreateOrEditEmployee;
  openBackdrop: boolean;
}

class ManageEmployee extends Component<
  IManageEmployeeProps,
  IManageEmployeeStates
> {
  constructor(props: IManageEmployeeProps) {
    super(props);
    this.state = {
      employee: {
        id: props.match.params.id,
        name: "",
        email: "",
        phone_number_1: "",
        phone_number_2: "",
        address_1: "",
        address_2: "",
        description: "",
        profile_picture: undefined,
        role: ""
      },
      openBackdrop: false
    };
  }

  public componentDidMount() {
    this.getEmployee();
  }

  public getEmployee(): void {
    this.setState({ openBackdrop: true });
    console.log("Getting an employee with id: " + this.state.employee.id);
    if (this.state.employee.id) {
      employeeService.getEmployeeForView(this.state.employee.id).then(res => {
        this.setState({employee: res.data});
        this.setState({ openBackdrop: false });
      });
    } else {
      this.setState({ openBackdrop: false });
    }
  }

  public updateInputValue = (event: any) => {
    let {employee} = this.state;
    employee = {
      ...employee,
      [event.target.id]: event.target.value
    };
    this.setState({employee: employee});
  };

  public onFileChangeHandler = (event: any) => {
    this.setState({ openBackdrop: true });
    const data = new FormData();
    data.append("file", event.target.files[0]);
    employeeService
      .uploadProfilePicture(this.state.employee.id, data)
      .then(result => {
        this.setState({
          employee: {...this.state.employee, profile_picture: result.data.data}
        });
        this.setState({ openBackdrop: false });
        this.props.enqueueSnackbar("Picture uploaded.", {variant: "success"});
      });
  };

  public deleteProfilePictureHandler = () => {
    this.setState({ openBackdrop: true });
    employeeService.deleteProfilePicture(this.state.employee.id).then(() => {
      this.setState({
        employee: {...this.state.employee, profile_picture: undefined}
      });
      this.setState({ openBackdrop: false });
      this.props.enqueueSnackbar("Profile Picture deleted.", {
        variant: "success"
      });
    });
  };

  public saveChangesHandler = () => {
    this.setState({ openBackdrop: true });
    employeeService.createOrEdit(this.state.employee).then(() => {
      this.setState({ openBackdrop: false });
      this.props.enqueueSnackbar("Changes saved.", {variant: "success"});
      this.props.history.push('/admin/manage-employees');
    });
  };

  public deleteEmployeeHandler = () => {
    this.setState({ openBackdrop: true });
    employeeService.delete(this.state.employee.id).then(() => {
      this.setState({ openBackdrop: false });
      this.props.enqueueSnackbar("Changes saved.", {variant: "success"});
      this.props.history.push('/admin/manage-employees');
    });
  }

  public actionComponents = (
    <div>
      <IconButton color="inherit" onClick={this.deleteEmployeeHandler}>
        <DeleteIcon />
      </IconButton>
    </div>
  );

  render() {
    const {employee, openBackdrop} = this.state;
    return (
      <div>
        <NavigationBar
          title={employee.id ? "Edit Employee" : "Add Employee"}
          actionComponents={employee.id ? this.actionComponents : null}
        />
        <div className="pa3">
          <Grid container spacing={3}>
            <Grid item xs={12} sm={12} md={6}>
              <div>
                <Typography paragraph variant="h6" color="primary">
                  Personal Details
                </Typography>
                <Typography paragraph variant="body1">
                  Full Name
                </Typography>
                <input
                  className="w-100 mb3"
                  id="name"
                  type="text"
                  placeholder="Full Name"
                  value={employee.name}
                  onChange={this.updateInputValue}
                />
                <Typography paragraph variant="body1">
                  Email
                </Typography>
                <input
                  className="w-100 mb3"
                  id="email"
                  type="email"
                  placeholder="Email"
                  value={employee.email}
                  onChange={this.updateInputValue}
                />
                <Typography paragraph variant="body1">
                  Primary Phone number
                </Typography>
                <input
                  className="w-100 mb3"
                  id="phone_number_1"
                  type="tel"
                  placeholder="Phone number 1"
                  value={employee.phone_number_1}
                  onChange={this.updateInputValue}
                />
                <Typography paragraph variant="body1">
                  Secondary Phone number
                </Typography>
                <input
                  className="w-100 mb3"
                  id="phone_number_2"
                  type="tel"
                  placeholder="Phone number 2"
                  value={employee.phone_number_2 || ""}
                  onChange={this.updateInputValue}
                />
                <Typography paragraph variant="body1">
                  Address 1
                </Typography>
                <input
                  className="w-100 mb3"
                  id="address_1"
                  type="text"
                  placeholder="Address 1"
                  value={employee.address_1}
                  onChange={this.updateInputValue}
                />
                <Typography paragraph variant="body1">
                  Address 2
                </Typography>
                <input
                  className="w-100 mb3"
                  id="address_2"
                  type="text"
                  placeholder="Address 2"
                  value={employee.address_2 || ""}
                  onChange={this.updateInputValue}
                />
                <Typography paragraph variant="body1">
                  Description
                </Typography>
                <input
                  className="w-100 mb3"
                  id="description"
                  type="text"
                  placeholder="Description"
                  value={employee.description || ""}
                  onChange={this.updateInputValue}
                />
                <Typography paragraph variant="body1">
                  Role
                </Typography>
                <input
                  className="w-100 mb3"
                  id="role"
                  type="text"
                  placeholder="Role"
                  value={employee.role || ""}
                  onChange={this.updateInputValue}
                />
              </div>
            </Grid>
            {employee.id ? (
              <Grid item xs={12} sm={12} md={6}>
                <div>
                  <Typography paragraph variant="h6" color="primary">
                    Profile Picture
                  </Typography>
                  {employee.profile_picture == null ? (
                    <p className="mb2">
                      No Profile Picture. Click the button below to upload.
                    </p>
                  ) : (
                    <div>
                      <img
                        className="br-100 h4 w4 mv3"
                        src={employee.profile_picture}
                        alt={employee.name}
                      />
                      <br />
                      <Button
                        variant="contained"
                        color="primary"
                        onClick={this.deleteProfilePictureHandler}
                      >
                        Delete Picture Picture
                      </Button>
                    </div>
                  )}
                  <br />
                  <input
                    className="db"
                    type="file"
                    name="file"
                    onChange={this.onFileChangeHandler}
                  />
                </div>
              </Grid>
            ) : null}
          </Grid>
        </div>
        <Backdrop open={openBackdrop} style={{ zIndex: 10 }}>
          <CircularProgress color="primary" />
        </Backdrop>
        <div className="fab">
          <Fab
            color="primary"
            aria-label="add"
            onClick={this.saveChangesHandler}
          >
            <SaveIcon />
          </Fab>
        </div>
      </div>
    );
  }
}

export default withSnackbar(ManageEmployee);
