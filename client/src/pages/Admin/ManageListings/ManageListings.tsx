import React, {Component} from "react";
import {Grid, Fab, CircularProgress} from "@material-ui/core/";
import PropertyCard from "../../../components/PropertyCard/PropertyCard";
import NavigationBar from "../../../components/Admin/NavigationBar/NavigationBar";
import AddIcon from "@material-ui/icons/Add";
import SearchIcon from "@material-ui/icons/Search";
import "../../../style.scss";
import {RouteComponentProps} from "react-router-dom";
import {GetPropertyOutput} from "../../../services/property/dto/getPropertyOutput";
import propertyService from "../../../services/property/propertyService";
import {GetAllPropertyInput} from "../../../services/property/dto/getAllPropertyInput";
import PagePagination from "../../../components/PagePagination/PagePagination";

interface IManageListingsProps extends RouteComponentProps<any> {}

interface IManageListingsStates {
  filterText: string;
  skipCount: number;
  maxResultCount: number;
  totalCount: number;
  pageCount: number;
  loading: boolean;
  properties: GetPropertyOutput[];
}

class ManageListings extends Component<
  IManageListingsProps,
  IManageListingsStates
> {
  constructor(props: IManageListingsProps) {
    super(props);
    this.state = {
      loading: false,
      filterText: "",
      skipCount: 0,
      maxResultCount: 12,
      totalCount: 0,
      pageCount: 0,
      properties: [],
    };
  }

  public updateFilterText = (event: any) => {
    this.setState({filterText: event.target.value});
  };

  public componentDidMount() {
    this.getListings();
  }

  public getListings = async (event?: any) => {
    if (event) event.preventDefault();
    this.setState({loading: true});
    const input: GetAllPropertyInput = {
      filter: this.state.filterText,
      maxResultCount: this.state.maxResultCount,
      skipCount: this.state.skipCount,
    };
    propertyService.getAll(input).then((res) => {
      this.setState({
        loading: false,
        properties: res.data.data,
        totalCount: res.data.totalCount,
        pageCount: Math.ceil(res.data.totalCount / this.state.maxResultCount),
      });
    });
  };

  public handlePageChange = async (event: any, value: number) => {
    const skipCount = this.state.maxResultCount * (value - 1);
    console.log(skipCount);
    await this.setState({skipCount: skipCount});
    this.getListings();
  };

  render() {
    const {properties, filterText, pageCount, loading} = this.state;
    return (
      <div>
        <NavigationBar title={"Manage Listings"} />
        <form className="m-3" onSubmit={this.getListings}>
          <input
            type="text"
            className="w-100"
            placeholder="Search"
            value={filterText}
            onChange={this.updateFilterText}
          />
        </form>
        {loading ? (
          <div className="vh-100 w-100 flex items-center justify-center">
            <CircularProgress />
          </div>
        ) : (
          <div className="p-3">
            <Grid container spacing={3}>
              {properties.map((item, i) => {
                return (
                  <Grid item xs={12} md={4} lg={3} key={i}>
                    <PropertyCard
                      data={item}
                      onClick={() =>
                        this.props.history.push(
                          `/admin/manage-listing/${item.id}`
                        )
                      }
                    />
                  </Grid>
                );
              })}
            </Grid>
          </div>
        )}
        <PagePagination
          pageCount={pageCount}
          onChange={this.handlePageChange}
        />
        <div className="fab">
          <Fab
            color="primary"
            aria-label="add"
            onClick={() => this.props.history.push("/admin/add-listing")}
          >
            <AddIcon />
          </Fab>
        </div>
      </div>
    );
  }
}

export default ManageListings;
