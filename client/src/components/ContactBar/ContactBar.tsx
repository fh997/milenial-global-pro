import React from 'react';
import './ContactBar.scss';

const ContactBar = () => {
	return (
		<section className='contactbar'>
			<div className='content py-5'>
				<div className='container'>
					<h5 className='contactbar-text pb-3'>Apakah anda ingin berbicara dengan ahli properti kami?</h5>
				    <a href='tel:+6282120699973' className='outlinedButtonLight'>0821 2069 9973</a>
	        	</div>
	        </div>
        </section>
	)
}

export default ContactBar;