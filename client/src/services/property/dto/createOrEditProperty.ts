export interface CreateOrEditProperty {
  id: number;
  title: string;
  description: string;
  bathrooms: number;
  bedrooms: number;
  garage: number;
  land_area: number;
  floor_area: number;
  price: number;
  address: string;
  city: string;
  postcode: number;
  province: string;
}
