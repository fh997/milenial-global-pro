import React, { Component } from "react";
import { Grid, Fab, Button, Backdrop, CircularProgress } from "@material-ui/core/";
import { withSnackbar, WithSnackbarProps } from "notistack";
import ImportExportIcon from "@material-ui/icons/ImportExport";
import NavigationBar from "../../../components/Admin/NavigationBar/NavigationBar";
import "../../../style.scss";
import { RouteComponentProps } from "react-router";
import propertyService from "../../../services/property/propertyService";

interface IImportListingsProps
  extends RouteComponentProps<any>,
    WithSnackbarProps {}

interface IImportListingsStates {
  openBackdrop: boolean;
}

class ImportListings extends Component<
  IImportListingsProps,
  IImportListingsStates
> {
  constructor(props: IImportListingsProps) {
    super(props);
    this.state = {
      openBackdrop: false
    }
  }

  componentDidMount() {
    // api.getUsers().then(res => this.setState({ users: res.data }))
  }

  public importListings = async () => {
    this.setState({ openBackdrop: true });
    propertyService.import().then(() => {
      this.setState({ openBackdrop: false });
    });
  }

  render() {
    return (
      <div className="import-listing">
        <NavigationBar title={"Import Listings"} />
        <div className="p-3">
          <h3>Import Listings</h3>
          <p>
            You can import listings posted on olx.com by pressing the import
            button below.
          </p>
          <Button variant="contained" color="primary" onClick={this.importListings}>
            Import
          </Button>
        </div>
        <Backdrop open={this.state.openBackdrop} style={{zIndex: 10}}>
          <CircularProgress color="inherit" />
        </Backdrop>
      </div>
    );
  }
}

export default withSnackbar(ImportListings);
