import React from "react";
import NavigationBar from "../NavigationBar/NavigationBar";
import "./JumbotronTitle.scss";

const Jumbotron = (props: {title: string}) => {
  const {title} = props;
  return (
    <header className="main">
      <div className="bg-black-40 jumbotron">
        <NavigationBar primary />
        <div className="container">
          <h1 className="jumbotron-title">{title}</h1>
        </div>
      </div>
    </header>
  );
};

export default Jumbotron;
