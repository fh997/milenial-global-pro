import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import Jumbotron from "../../components/Jumbotron/Jumbotron";
import ContactBar from "../../components/ContactBar/ContactBar";
import Footer from "../../components/Footer/Footer";
import PhoneIcon from "@material-ui/icons/Phone";
import EmailIcon from "@material-ui/icons/Email";
import OfficeLocation from "../../assets/images/office_location.png";
import "./ContactUs.scss";

class ContactUs extends Component {
  componentDidMount() {
    document.title = "Contact Us | Milenial Global Pro";
  }

  render() {
    return (
      <div className="App">
        <Jumbotron title="Contact Us" />
        <div className="container py-5">
          <div className="company-info text-center">
            <div className="mb-5">
              <h4>Kantor Kami</h4>
              <p>Ruko Mekar Utama 1 No. 12</p>
              <p>Komp Mekar Wangi</p>
              <p>Bandung, Indonesia 40237</p>
            </div>
            <div>
              <h4>Contact Information</h4>
              <p className="mb3">
                <a href="tel:+6282120699973">0821 2069 9973 / 0815 606 5512</a>
              </p>
              <a href="mailto:millenials.propertindo@gmail.com">
                millenials.propertindo@gmail.com
              </a>
            </div>
            <div className="text-center mt-5">
              <img src={OfficeLocation} alt="Office Location" />
            </div>
          </div>
        </div>
        <ContactBar />
        <Footer />
      </div>
    );
  }
}

export default ContactUs;
