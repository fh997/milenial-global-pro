import React from "react";
import {
  Grid,
  Card,
  IconButton,
  CardMedia,
  CardActions,
  Typography,
} from "@material-ui/core/";
import DeleteIcon from "@material-ui/icons/Delete";
import {GetPropertyPictureOutput} from "../../../services/property_picture/dto/getPropertyPictureOutput";

const PictureGrid = (props: {
  images: GetPropertyPictureOutput[];
  onDelete: any;
}) => {
  const {images, onDelete} = props;
  return (
    <div className="mv2">
      <Grid container spacing={1}>
        {images.map((image, i) => {
          return (
            <Grid item xs={12} sm={6} md={4} lg={3} key={i}>
              <Card>
                <CardMedia
                  component="img"
                  alt={`property-${i}`}
                  height="200"
                  image={image.url_path}
                />
                <CardActions>
                  <Typography variant="body1" noWrap>
                    {image.filename}
                  </Typography>
                  <IconButton onClick={() => onDelete(image.id)}>
                    <DeleteIcon />
                  </IconButton>
                </CardActions>
              </Card>
            </Grid>
          );
        })}
      </Grid>
    </div>
  );
};

export default PictureGrid;
