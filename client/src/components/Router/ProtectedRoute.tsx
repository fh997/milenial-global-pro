import * as React from "react";

import {Redirect, Route} from "react-router-dom";
import authenticationService from "../../services/authentication/authenticationService";

const ProtectedRoute = ({
  path,
  component: Component,
  permission,
  render,
  ...rest
}: any) => {
  return (
    <Route
      {...rest}
      render={props => {
        if (!authenticationService.getCurrentUser()) {
          return (
            <Redirect
              to={{
                pathname: "/admin",
                state: {from: props.location}
              }}
            />
          );
        }

        return Component ? <Component {...props} /> : render(props);
      }}
    />
  );
};

export default ProtectedRoute;
