import { GetAllPropertyInput } from "./dto/getAllPropertyInput";
import { CreateOrEditProperty } from "./dto/createOrEditProperty";
import http from "../httpService";
import authenticationService from "../authentication/authenticationService";

class PropertyService {
  public async getAll(input: GetAllPropertyInput) {
    let result = await http.get("property/GetAll", { params: input });
    return result;
  }

  public async getPropertyForView(propertyId: number) {
    let result = await http.get("property/GetPropertyForView", {
      params: { id: propertyId }
    });
    return result;
  }

  public async createOrEdit(input: CreateOrEditProperty) {
    let result = await http.post("property/CreateOrEdit", input, {
      headers: authenticationService.authHeader()
    });
    return result;
  }

  public async import() {
    let result = await http.post("property/ImportProperties", null, {
      headers: authenticationService.authHeader()
    });
    return result;
  }

  public async delete(propertyId: number) {
    let result = await http.delete("property/Delete", {
      headers: authenticationService.authHeader(),
      params: { id: propertyId }
    });
    return result;
  }
}

export default new PropertyService();
