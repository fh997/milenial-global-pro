import React, {Component} from "react";
import {Grid, LinearProgress, CircularProgress} from "@material-ui/core/";
import Jumbotron from "../../components/Jumbotron/Jumbotron";
import ContactBar from "../../components/ContactBar/ContactBar";
import SearchSidebar from "../../components/SearchSidebar/SearchSidebar";
import PropertyCard from "../../components/PropertyCard/PropertyCard";
import Footer from "../../components/Footer/Footer";
import SearchIcon from "@material-ui/icons/Search";
import "./Listings.scss";
import {RouteComponentProps} from "react-router-dom";
import propertyService from "../../services/property/propertyService";
import {GetAllPropertyInput} from "../../services/property/dto/getAllPropertyInput";
import {GetPropertyOutput} from "../../services/property/dto/getPropertyOutput";
import PagePagination from "../../components/PagePagination/PagePagination";
import qs from "query-string";

interface IListingsProps extends RouteComponentProps<any> {}

interface IListingsStates {
  properties: GetPropertyOutput[];
  skipCount: number;
  maxResultCount: number;
  totalCount: number;
  pageCount: number;
  loading: boolean;
}

class Listings extends Component<IListingsProps, IListingsStates> {
  constructor(props: IListingsProps) {
    super(props);
    this.state = {
      properties: [],
      skipCount: 0,
      maxResultCount: 8,
      totalCount: 0,
      pageCount: 0,
      loading: true,
    };
  }

  public componentDidMount() {
    document.title = "Listings | Milenial Global Pro";

    // const url = new URL("http://localhost:5000" + params);
    // const searchQuery = url.searchParams.get("query");

    // console.log("params");
    // console.log(searchQuery);

    // console.log(params.get("query"));
    // console.log(Number(params.get("bedrooms")));

    this.getListings();
  }

  public getListings = async (event?: any) => {
    if (event) event.preventDefault();

    const searchParams = qs.parse(this.props.location.search, {parseBooleans: true, parseNumbers: true});

    // for (let param in searchParams) {
    //   if (param == "") param = true;
    // }

    const getAllInput: GetAllPropertyInput = {
      ...searchParams,
      maxResultCount: this.state.maxResultCount,
      skipCount: this.state.skipCount,
    };

    // if (getAllInput.minBedrooms)
    //   getAllInput.minBedrooms = Number(getAllInput.minBedrooms);
    // if (getAllInput.minBathrooms)
    //   getAllInput.minBathrooms = Number(getAllInput.minBathrooms);
    // if (getAllInput.minPrice)
    //   getAllInput.minPrice = Number(getAllInput.minPrice);
    // if (getAllInput.maxPrice)
    //   getAllInput.maxPrice = Number(getAllInput.maxPrice);

    console.log("input");
    console.log(getAllInput);

    propertyService.getAll(getAllInput).then((result) => {
      console.log("result");
      console.log(result);
      this.setState({
        loading: false,
        properties: result.data.data,
        totalCount: result.data.totalCount,
        pageCount: Math.ceil(
          result.data.totalCount / this.state.maxResultCount
        ),
      });
    });
  };

  public handlePageChange = async (event: any, value: number) => {
    const skipCount = this.state.maxResultCount * value - 1;
    await this.setState({skipCount: skipCount});
    this.getListings();
  };

  public navigateToProperty = (id: number) => {
    this.props.history.push(`/listing/${id}`);
  }

  render() {
    const {properties, loading, pageCount} = this.state;
    return (
      <div className="listings-page">
        <Jumbotron title="Listings" />
        <div className="py-5 container">
          <Grid container spacing={5}>
            <Grid item xs={12} sm={5} md={4}>
              <div className="pv4">
                <SearchSidebar />
              </div>
            </Grid>
            <Grid item xs={12} sm={7} md={8}>
              {loading ? (
                <div className="vh-100 w-100 flex items-center justify-center">
                  <CircularProgress />
                </div>
              ) : (
                <div className="mb-5">
                  <Grid container spacing={2}>
                    {properties.length === 0 ? (
                      <div>
                        <SearchIcon className="mt2" fontSize="large" />
                        <h2 className="pv2">Not found</h2>
                        <p>
                          There is no property that matches your search
                          criteria. Please change your search criteria.
                        </p>
                      </div>
                    ) : (
                      properties.map((entry, i) => {
                        return (
                          <Grid item xs={12} md={6} key={i}>
                            <PropertyCard
                              onClick={() => this.navigateToProperty(entry.id)}
                              data={entry}
                            />
                          </Grid>
                        );
                      })
                    )}
                  </Grid>
                </div>
              )}
              <PagePagination
                pageCount={pageCount}
                onChange={this.handlePageChange}
              />
            </Grid>
          </Grid>
        </div>
        <ContactBar />
        <Footer />
      </div>
    );
  }
}

export default Listings;
