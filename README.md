# Milenial Global Pro

## Description
Milenial Global Pro is a property agent company in Indonesia, specialising in selling and renting houses, buildings, warehouses, land, and apartments. The company needs a website to host all the property listing for the potential buyers or renters to contact the agents. The website was primarily built using javascript technologies such as React.js and Node.js for the server side. For the database side, it uses MySQL and the website is hosted on Amazon web services.

Main Features:

- Property listing which includes information such as bedrooms, bathrooms, location, and description.

- Administrator functionalities such as editing, creating, deleting properties, employees, and users.

- Search ability for the users for properties

- Import/Sync functionality from other website such as olx.com

## Quick Start
To get started with the development of the web application, you must install docker as the system is containerised. For each component, client and server, it has its own dockerfile which will build a container. Then, the docker compose file will combine all the containers exist.
```
# This command is to build the docker compose
docker-compose build --no-cache

# This command is to start the service
docker-compose up

# This command is to stop the service
docker-compose stop
```

The server and the client can be accessed at http://localhost:3000 and http://localhost:5000

## Deployment
The deployment is done using heroku container registry. The following commands are executed when the application is being deployed to 

The live website can be accessed at http://milenialglobalpro.com

```
# Build docker compose production
docker-compose -f docker-compose.yml build

# Build docker compose development
docker-compose -f docker-compose.dev.yml build

# Up docker compose
docker-compose up

# Deploy the frontend using container
cd ./client
heroku container:push web --app milenialglobalpro
heroku container:release web

# Deploy the backend using container
cd ./server
heroku container:push web --app milenialglobalpro-server
heroku container:release web

# Heroku deployment
heroku stack:set container
```