import React from "react";
import Carousel from "react-material-ui-carousel";
import "./PropertyImagesCarousel.scss";

const PropertyImagesCarousel = ({images}) => {
  return (
    <div className="property-images-carousel">
      <Carousel autoPlay={false}>
        {images.map((item, i) => {
          return (
            <img
              key={i}
              className="carousel-image"
              src={item.url_path}
              alt={`property-${i + 1}`}
            />
          );
        })}
      </Carousel>
    </div>
  );
};

export default PropertyImagesCarousel;
