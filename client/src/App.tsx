import React, {Component} from "react";
import {Route, Switch} from "react-router-dom";
import {withSnackbar, WithSnackbarProps} from "notistack";
import Home from "./pages/Home/Home";
import About from "./pages/About/About";
import ContactUs from "./pages/ContactUs/ContactUs";
import Listings from "./pages/Listings/Listings";
import Listing from "./pages/Listing/Listing";
import NotFound from "./pages/NotFound/NotFound";
import ManageListings from "./pages/Admin/ManageListings/ManageListings";
import ManageListing from "./pages/Admin/ManageListing/ManageListing";
import ManageEmployees from "./pages/Admin/ManageEmployees/ManageEmployees";
import ManageEmployee from "./pages/Admin/ManageEmployee/ManageEmployee";
import Login from "./pages/Admin/Login/Login";
import ManageUsers from "./pages/Admin/ManageUsers/ManageUsers";
import ManageUser from "./pages/Admin/ManageUser/ManageUser";
import EditDetails from "./pages/Admin/EditDetails/EditDetails";
import ProtectedRoute from "./components/Router/ProtectedRoute";
import ImportListings from "./pages/Admin/ImportListings/ImportListings";

interface IAppProps extends WithSnackbarProps {}

class App extends Component<IAppProps> {
  render() {
    return (
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/about" component={About} />
        <Route path="/contact-us" component={ContactUs} />
        <Route path="/listings" component={Listings} />
        <Route path="/listing/:id" component={Listing} />
        <Route exact path="/admin" component={Login} />
        <ProtectedRoute
          path="/admin/manage-listings"
          component={ManageListings}
        />
        <ProtectedRoute
          path="/admin/manage-listing/:id"
          component={ManageListing}
        />
        <ProtectedRoute path="/admin/add-listing" component={ManageListing} />
        <ProtectedRoute
          path="/admin/manage-employees"
          component={ManageEmployees}
        />
        <ProtectedRoute
          path="/admin/manage-employee/:id"
          component={ManageEmployee}
        />
        <ProtectedRoute path="/admin/add-employee" component={ManageEmployee} />
        <ProtectedRoute path="/admin/manage-users" component={ManageUsers} />
        <ProtectedRoute path="/admin/manage-user/:id" component={ManageUser} />
        <ProtectedRoute path="/admin/add-user" component={ManageUser} />
        <ProtectedRoute path="/admin/edit-details" component={EditDetails} />
        <ProtectedRoute path="/admin/import-listings" component={ImportListings} />
        <Route component={NotFound} />
      </Switch>
    );
  }
}

export default withSnackbar(App);
