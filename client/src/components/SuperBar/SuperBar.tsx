import React from "react";
import "./SuperBar.scss";

const SuperBar = () => {
  return (
    <section className="superbar">
      <a href="/contact-us" className="btn">
        <h5>
          Beli property yang anda impikan sekarang juga! Kontak kami untuk
          konsultasi
        </h5>
      </a>
    </section>
  );
};

export default SuperBar;
