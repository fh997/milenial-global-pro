export interface GetEmployeeOutput {
  id: number;
  name: string;
  email: string;
  phone_number_1: string;
  phone_number_2: string;
  address_1: string;
  address_2: string;
  profile_picture: string;
  description: string;
  role: number;
}
