const express = require("express");
const { protect } = require('../middleware/auth');
const {
  getAll,
  getPropertyPictureForView,
  create,
  deletePropertyPicture
} = require("../controllers/property_pictures");

const router = express.Router();

router.route("/GetAll").get(getAll);

router.route("/GetPropertyPictureForView").get(getPropertyPictureForView);

router.route("/Create").post(protect, create);

router.route("/Delete").delete(protect, deletePropertyPicture);

module.exports = router;
