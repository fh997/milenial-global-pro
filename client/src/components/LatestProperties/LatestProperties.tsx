import React from "react";
import {Grid} from "@material-ui/core/";
import PropertyCard from "../PropertyCard/PropertyCard";
import "./LatestProperties.scss";
import { useHistory } from "react-router-dom";

const LatestProperties = (props: {data: any[]}) => {
  let history = useHistory();

  const {data} = props;

  const navigateToProperty = (id: number) => {
    history.push(`/listing/${id}`);
  }

  return (
    <section className="latest-properties">
      <div className="container">
        <h4 className="pb-4 section-text text-center">Latest Listing</h4>
        <Grid container spacing={2}>
          {data.slice(0, 3).map((item, i) => {
            return (
              <Grid item key={i} xs={12} sm={12} md={4}>
                <PropertyCard data={item} onClick={() => navigateToProperty(item.id)}/>
              </Grid>
            );
          })}
        </Grid>
      </div>
    </section>
  );
};

export default LatestProperties;
