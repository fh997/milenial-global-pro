import React, {Component} from "react";
import {
  Fab,
  Typography,
  IconButton,
  Backdrop,
  CircularProgress,
  Tabs,
  Tab,
} from "@material-ui/core/";
import {withSnackbar, WithSnackbarProps} from "notistack";
import SaveIcon from "@material-ui/icons/Save";
import DeleteIcon from "@material-ui/icons/Delete";
import NavigationBar from "../../../components/Admin/NavigationBar/NavigationBar";
import PictureGrid from "../../../components/Admin/PictureGrid/PictureGrid";
import "../../../style.scss";
import {RouteComponentProps} from "react-router-dom";
import propertyService from "../../../services/property/propertyService";
import {GetPropertyOutput} from "../../../services/property/dto/getPropertyOutput";
import {GetPropertyPictureOutput} from "../../../services/property_picture/dto/getPropertyPictureOutput";
import propertyPictureService from "../../../services/property_picture/propertyPictureService";
import {Editor} from "@tinymce/tinymce-react";
import TabPanel from "../../../components/Admin/TabPanel/TabPanel";

interface IManageListingProps
  extends RouteComponentProps<any>,
    WithSnackbarProps {}

interface IManageListingStates {
  property: GetPropertyOutput;
  propertyPictures: GetPropertyPictureOutput[];
  openBackdrop: boolean;
  currentTabIndex: number;
}

class ManageListing extends Component<
  IManageListingProps,
  IManageListingStates
> {
  constructor(props: IManageListingProps) {
    super(props);
    this.state = {
      property: {
        id: props.match.params.id,
        title: "",
        description: "",
        short_description: "",
        bathrooms: 0,
        bedrooms: 0,
        garage: 0,
        land_area: 0,
        floor_area: 0,
        price: 0,
        address: "",
        city: "",
        postcode: 0,
        province: "",
      },
      propertyPictures: [],
      currentTabIndex: 0,
      openBackdrop: false,
    };
  }

  public componentDidMount() {
    this.getProperty();
    this.getPropertyPictures();
  }

  public getProperty = () => {
    this.setState({openBackdrop: true});
    propertyService.getPropertyForView(this.state.property.id).then((res) => {
      this.setState({property: res.data});
      console.log(this.state.property);
    });
  };

  public getPropertyPictures = () => {
    propertyPictureService
      .getAll({propertyId: this.state.property.id})
      .then((res) => {
        this.setState({propertyPictures: res.data});
        this.setState({openBackdrop: false});
      });
  };

  public updateInputValue = (event: any) => {
    this.setState({
      property: {
        ...this.state.property,
        [event.target.id]: event.target.value,
      },
    });
  };

  public onDeleteImageHandler = (id: number) => {
    this.setState({openBackdrop: true});
    propertyPictureService.delete(id).then(() => {
      this.setState({openBackdrop: false});
      this.props.enqueueSnackbar("Picture deleted.", {variant: "success"});
      this.getPropertyPictures();
    });
  };

  public onFileChangeHandler = (event: any) => {
    this.setState({openBackdrop: true});
    // const files = Array.from(event.target.files);
    const formData = new FormData();
    formData.append("file", event.target.files[0]);
    // files.forEach((file: any, i) => {
    //   formData.append("file", file);
    // });
    propertyPictureService
      .create(this.state.property.id, formData)
      .then((res) => {
        // this.state.propertyPictures.push(res.data.data);
        // this.setState({propertyPictures: this.state.propertyPictures});
        this.setState({openBackdrop: false});
        this.props.enqueueSnackbar("Picture uploaded.", {variant: "success"});
        this.getPropertyPictures();
      });
  };

  public saveListingHandler = () => {
    this.setState({openBackdrop: true});
    propertyService.createOrEdit(this.state.property).then(() => {
      this.setState({openBackdrop: false});
      this.props.enqueueSnackbar("Changes saved.", {variant: "success"});
      this.props.history.push("/admin/manage-listings");
    });
  };

  public deleteListingHandler = () => {
    this.setState({openBackdrop: true});
    propertyService.delete(this.state.property.id).then(() => {
      this.setState({openBackdrop: false});
      this.props.enqueueSnackbar("Deleted.", {variant: "success"});
      this.props.history.push("/admin/manage-listings");
    });
  };

  public actionComponents = (
    <IconButton color="inherit" onClick={this.deleteListingHandler}>
      <DeleteIcon />
    </IconButton>
  );

  public handleEditorChange = (content: string, editor: any) => {
    this.setState({property: {...this.state.property, description: content}});
  };

  public handleTabIndexChange = (event: any, value: number) => {
    this.setState({currentTabIndex: value});
  };

  render() {
    const {
      property,
      propertyPictures,
      openBackdrop,
      currentTabIndex,
    } = this.state;
    return (
      <div>
        <NavigationBar
          title={property.id ? "Edit Listing" : "Add Listing"}
          actionComponents={property.id ? this.actionComponents : null}
          tabBar={
            <Tabs value={currentTabIndex} onChange={this.handleTabIndexChange}>
              <Tab label="Details" />
              <Tab label="Descriptions" />
              <Tab label="Pictures" />
            </Tabs>
          }
        />
        <TabPanel value={currentTabIndex} index={0}>
          <div>
            <Typography paragraph={true} variant="h6" color="primary">
              Property Details
            </Typography>
            <div>
              <Typography paragraph variant="body1">
                Listing title
              </Typography>
              <input
                className="mb3 w-100"
                id="title"
                type="text"
                placeholder="Listing title"
                value={property.title}
                onChange={this.updateInputValue}
                required
              />
              <Typography paragraph={true} variant="body1">
                Location
              </Typography>
              <input
                className="mb3 w-100"
                id="province"
                type="text"
                placeholder="Property location"
                value={property.province}
                onChange={this.updateInputValue}
              />
              <Typography paragraph={true} variant="body1">
                Address
              </Typography>
              <input
                className="mb3 w-100"
                id="address"
                type="text"
                placeholder="Property address"
                value={property.address}
                onChange={this.updateInputValue}
              />
              <Typography paragraph={true} variant="body1">
                City
              </Typography>
              <input
                className="mb3 w-100"
                id="city"
                type="text"
                placeholder="City"
                value={property.city}
                onChange={this.updateInputValue}
              />
              <Typography paragraph={true} variant="body1">
                Postcode
              </Typography>
              <input
                className="mb3 w-100"
                id="postcode"
                type="text"
                placeholder="Postcode"
                value={property.postcode}
                onChange={this.updateInputValue}
              />
              <Typography paragraph={true} variant="body1">
                Price
              </Typography>
              <input
                className="mb3 w-100"
                id="price"
                type="number"
                placeholder="Property price"
                value={property.price}
                onChange={this.updateInputValue}
              />
              <Typography paragraph={true} variant="body1">
                Number of bedrooms
              </Typography>
              <input
                className="mb3 w-100"
                id="bedrooms"
                type="text"
                placeholder="Number of bedrooms"
                value={property.bedrooms}
                onChange={this.updateInputValue}
              />
              <Typography paragraph={true} variant="body1">
                Number of bathrooms
              </Typography>
              <input
                className="mb3 w-100"
                id="bathrooms"
                type="text"
                placeholder="Number of bathrooms"
                value={property.bathrooms}
                onChange={this.updateInputValue}
              />
              <Typography paragraph={true} variant="body1">
                Luas tanah
              </Typography>
              <input
                className="mb3 w-100"
                id="land_area"
                type="number"
                placeholder="Luas Tanah"
                value={property.land_area}
                onChange={this.updateInputValue}
              />
              <Typography paragraph={true} variant="body1">
                Luas bangunan
              </Typography>
              <input
                className="mb3 w-100"
                id="floor_area"
                type="number"
                placeholder="Luas Bangunan"
                value={property.floor_area}
                onChange={this.updateInputValue}
              />
              <Typography paragraph={true} variant="body1">
                Short Description
              </Typography>
              <textarea
                className="w-100"
                id="short_description"
                placeholder="Short Description"
                value={property.short_description}
                onChange={this.updateInputValue}
              />
            </div>
          </div>
        </TabPanel>
        <TabPanel value={currentTabIndex} index={1}>
          <div>
            <div>
              <Typography paragraph={true} variant="h6" color="primary">
                Description
              </Typography>
              <Editor
                apiKey="7s4ekf1v56feynwgkx8iz21hc2fpvc98fphf8uj2u2oib2yq"
                value={property.description}
                init={{
                  height: 500,
                  menubar: false,
                  plugins: [
                    "advlist autolink lists link image",
                    "charmap print preview anchor help",
                    "searchreplace visualblocks code",
                    "insertdatetime media table paste wordcount",
                  ],
                  toolbar:
                    "undo redo | formatselect | bold italic | \
            alignleft aligncenter alignright | \
            bullist numlist outdent indent | help",
                }}
                onEditorChange={this.handleEditorChange}
              />
            </div>
          </div>
        </TabPanel>
        <TabPanel value={currentTabIndex} index={2}>
          {property.id ? (
            <div className="mb6">
              <Typography paragraph={true} variant="h6" color="primary">
                Property Images
              </Typography>
              <input
                onChange={this.onFileChangeHandler}
                className="db w-100"
                type="file"
                name="file"
                accept="image/*"
                multiple
              />
              <PictureGrid
                images={propertyPictures}
                onDelete={this.onDeleteImageHandler}
              />
            </div>
          ) : null}
        </TabPanel>
        {/* <div className="pa3">
          <Grid container spacing={2}>
            <Grid item xs={12} sm={12} md={6}></Grid>
          </Grid>
        </div> */}
        <Backdrop open={openBackdrop} style={{zIndex: 10}}>
          <CircularProgress color="inherit" />
        </Backdrop>
        <div className="fab">
          <Fab
            color="primary"
            aria-label="add"
            onClick={this.saveListingHandler}
          >
            <SaveIcon />
          </Fab>
        </div>
      </div>
    );
  }
}

export default withSnackbar(ManageListing);
