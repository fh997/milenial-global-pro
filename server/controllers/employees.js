const asyncHandler = require("../middleware/async");
const db = require("../config/db");
const path = require("path");
const fs = require("fs");
const cloudinary = require('cloudinary').v2;

/**
 * @swagger
 * path:
 *  /api/v1/employee/GetAll:
 *    get:
 *      summary: Get all employees
 *      tags: [Employees]
 *      produces:
 *          - application/json
 *      parameters:
 *        - name: filter
 *          in: query
 *          description: Filter for search query.
 *          required: false
 *          type: string
 *        - name: skipCount
 *          in: query
 *          description: skip count for paging.
 *          required: false
 *          type: integer
 *        - name: maxResultCount
 *          in: query
 *          description: Maximum result count for paging.
 *          required: false
 *          type: integer
 *      responses:
 *        "200":
 *          description: Returns all employees on the system
 */

exports.getAll = asyncHandler(async (req, res, next) => {
  let {
    filter,
    maxResultCount,
    skipCount
  } = req.query;
  db.select(["employees.id as id", "employees.name as name", "role_id", "email", "phone_number_1", "phone_number_2", "address_1", "address_2", "description", "role", "profile_picture"])
    .table("employees")
    .leftOuterJoin("users", "employees.user_id", "users.id")
    .modify(db => (maxResultCount != null ? db.limit(maxResultCount) : true))
    .modify(db => (skipCount != null ? db.offset(skipCount) : true))
    .modify(db =>
      filter != null ?
      db.whereRaw(
        "LOWER(name) LIKE '%' || LOWER(?) || '%' ",
        filter.toLowerCase()
      ) :
      true
    )
    .orderBy([{
      column: "users.role_id",
      order: "asc"
    }])
    .then(result => {
      const response = {
        status: "success",
        data: result
      };
      return res.status(200).json(response);
    });
});

/**
 * @swagger
 * path:
 *  /api/v1/employee/GetEmployeeForView:
 *    get:
 *      summary: Get a single employee
 *      tags: [Employees]
 *      produces:
 *          - application/json
 *      parameters:
 *        - name: employeeId
 *          in: query
 *          description: Employee id
 *          required: true
 *          type: integer
 *      responses:
 *        "200":
 *          description: Returns an employee with id given
 */

exports.getEmployeeForView = asyncHandler(async (req, res, next) => {
  db.select()
    .table("employees")
    .where("id", "=", req.query.employeeId)
    .then(result => {
      return res.status(200).json(result[0]);
    });
});

/**
 * @swagger
 * path:
 *  /api/v1/employee/CreateOrEdit:
 *    post:
 *      summary: Insert or edit an employee
 *      tags: [Employees]
 *      produces:
 *          - application/json
 *      parameters:
 *        - name: Employee
 *          in: body
 *          description: Employee object
 *          required: true
 *          type: object
 *      responses:
 *        "200":
 *          description: Create or edit employee
 */

exports.createOrEdit = asyncHandler(async (req, res, next) => {
  if (req.body.id == null) {
    insertEmployee(req, res, next);
  } else {
    updateEmployee(req, res, next);
  }
});

const insertEmployee = asyncHandler(async (req, res, next) => {
  db("employees")
    .insert(req.body)
    .then(() => {
      return res.status(200).json({
        success: true
      });
    })
    .catch(err => {
      return res.status(500).json({
        success: false,
        error: err.toString()
      });
    });
});

const updateEmployee = asyncHandler(async (req, res, next) => {
  db("employees")
    .where("id", "=", req.body.id)
    .update(req.body)
    .then(() => {
      return res.status(200).json({
        success: true
      });
    })
    .catch(err => {
      return res.status(500).json({
        success: false,
        error: err.toString()
      });
    });
});

/**
 * @swagger
 * path:
 *  /api/v1/employee/UploadProfilePicture:
 *    post:
 *      summary: Upload or edit profile picture of an employee
 *      tags: [Employees]
 *      produces:
 *          - application/json
 *      parameters:
 *        - name: employeeId
 *          in: query
 *          description: employee id
 *          required: true
 *          type: integer
 *        - name: file
 *          in: file
 *          description: Employee's profile picture
 *          required: true
 *          type: file
 *      responses:
 *        "200":
 *          description: Success
 */

exports.uploadProfilePicture = asyncHandler(async (req, res, next) => {
  const {
    employeeId
  } = req.query;
  const file = req.files.file;

  if (!req.files) {
    return next(new ErrorResponse(`Please upload a file`, 400));
  }

  // Make sure the image is a photo
  if (!file.mimetype.startsWith('image')) {
    return next(new ErrorResponse(`Please upload an image file`, 400));
  }

  // Check filesize
  if (file.size > process.env.MAX_FILE_UPLOAD) {
    return next(
      new ErrorResponse(
        `Please upload an image less than ${process.env.MAX_FILE_UPLOAD}`,
        400
      )
    );
  }

  // Delete existing photo

  db.select()
    .table("employees")
    .where("id", "=", req.query.employeeId)
    .then(result => {
      const employee = result[0];
      if (employee.profile_picture) {
        const filePath =
          process.env.FILE_UPLOAD_PATH +
          String(employee.profile_picture).replace(
            process.env.FILE_UPLOAD_URL,
            ""
          );

        const fileName = String(employee.profile_picture).replace(
          process.env.FILE_UPLOAD_URL,
          ""
        );

        const public_id = path.parse(fileName).name;

        cloudinary.uploader.destroy(public_id, {}, () => {});

        fs.unlink(filePath, () => {});
      }
    });

  // Create custom filename

  const fileName = `photo_${employeeId}_${Date.now()}`;

  file.name = fileName + path.parse(file.name).ext;

  file.mv(`${process.env.FILE_UPLOAD_PATH}/${file.name}`, async err => {
    if (err) {
      console.error(err);
      return next(new ErrorResponse(`Problem with file upload`, 500));
    }

    let fileUrl = `${process.env.FILE_UPLOAD_URL}${file.name}`;

    cloudinary.uploader.upload(`${process.env.FILE_UPLOAD_PATH}/${file.name}`, {
        public_id: fileName
      },
      function (error, result) {
        if (!error) {
          fileUrl = result.url;
          db("employees")
            .where("id", "=", employeeId)
            .update("profile_picture", fileUrl)
            .then(() => {
              return res.status(200).json({
                success: true,
                data: fileUrl
              });
            })
            .catch(err => {
              return res.status(500).json({
                success: false,
                error: err.toString()
              });
            });
        }
      }
    );


  });
});

/**
 * @swagger
 * path:
 *  /api/v1/employee/DeleteProfilePicture:
 *    delete:
 *      summary: Delete the profile picture of an employee
 *      tags: [Employees]
 *      produces:
 *          - application/json
 *      parameters:
 *        - name: employeeId
 *          in: query
 *          description: Employee id
 *          required: true
 *          type: integer
 *      responses:
 *        "200":
 *          description: Success
 */

exports.deleteProfilePicture = asyncHandler(async (req, res, next) => {
  db
    .select()
    .table("employees")
    .where("id", "=", req.query.employeeId)
    .then(result => {
      if (result.length == 0) {
        return res.status(200).json({
          success: false
        });
      }

      const employee = result[0];

      const filePath =
        process.env.FILE_UPLOAD_PATH +
        String(employee.profile_picture).replace(
          process.env.FILE_UPLOAD_URL,
          ""
        );

      const fileName = String(employee.profile_picture).replace(
        process.env.FILE_UPLOAD_URL,
        ""
      );

      const public_id = path.parse(fileName).name;

      cloudinary.uploader.destroy(public_id, {}, () => {});

      fs.unlink(filePath, () => {});

      db("employees")
        .where("id", "=", employee.id)
        .update("profile_picture", null)
        .then(() => {
          return res.status(200).json({
            success: true
          });
        })
        .catch(err => {
          return res.status(500).json({
            success: false,
            error: err.toString()
          });
        });
    });
});

/**
 * @swagger
 * path:
 *  /api/v1/employee/Delete:
 *    delete:
 *      summary: Delete an employee
 *      tags: [Employees]
 *      produces:
 *          - application/json
 *      parameters:
 *        - name: id
 *          in: query
 *          description: Employee id
 *          required: true
 *          type: integer
 *      responses:
 *        "200":
 *          description: Delete an employee with id given
 */

exports.deleteEmployee = asyncHandler(async (req, res, next) => {
  db("employees")
    .where("id", "=", req.query.id)
    .del()
    .then(() => {
      return res.status(200).json({
        success: true
      });
    })
    .catch(err => {
      return res.status(500).json({
        success: false,
        error: err.toString()
      });
    });
});