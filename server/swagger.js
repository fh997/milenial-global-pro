const swaggerJsdoc = require("swagger-jsdoc");
const options = {
  // List of files to be processed.
  apis: [
    "./controllers/auth.js",
    "./controllers/employees.js",
    "./controllers/properties.js",
    "./controllers/property_pictures.js"
  ],
  // You can also set globs for your apis
  // e.g. './routes/*.js'
  basePath: "/",
  swaggerDefinition: {
    info: {
      description: "Milenial Global Pro API Documentation",
      swagger: "2.0",
      title: "Milenial Global Pro",
      version: "1.0.0"
    },
    components: {
      securitySchemes: {
        bearerAuth: {
          type: 'http',
          scheme: 'bearer',
          bearerFormat: 'JWT',
        }
      }
    },
    securityDefinitions: {
      auth: {
        type: 'basic'
      }
    },
    security: [{
      bearerAuth: []
    }],
    openapi: '3.0.1'
  }
};
const specs = swaggerJsdoc(options);
module.exports = specs;