export interface GetAllPropertyInput {
    filter?: string;
    minBathrooms?: number;
    maxBathrooms?: number;
    minBedrooms?: number;
    maxBedrooms?: number;
    minGarages?: number;
    maxGarages?: number;
    minLandArea?: number;
    maxLandArea?: number;
    minFloorArea?: number;
    maxFloorArea?: number;
    minPrice?: number;
    maxPrice?: number;
    city?: string;
    province?: string;
    sorting?: string;
    skipCount?: number;
    maxResultCount?: number;
}