import axios from "axios";

const qs = require("qs");

const http = axios.create({
  baseURL: `${process.env.REACT_APP_SERVER_ROOT_ADDRESS}/api/v1/`,
  timeout: 30000,
  paramsSerializer: function(params) {
    return qs.stringify(params, {
      encode: false
    });
  }
});

export default http;
