const express = require("express");
const { protect } = require('../middleware/auth');

const { getAll, getEmployeeForView, createOrEdit, deleteEmployee, uploadProfilePicture, deleteProfilePicture } = require("../controllers/employees");

const router = express.Router();

router.route("/GetAll").get(getAll);

router.route("/GetEmployeeForView").get(getEmployeeForView);

router.route("/CreateOrEdit").post(protect, createOrEdit);

router.route("/UploadProfilePicture").post(protect, uploadProfilePicture);

router.route("/DeleteProfilePicture").delete(protect, deleteProfilePicture)

router.route("/Delete").delete(protect, deleteEmployee);

module.exports = router;
