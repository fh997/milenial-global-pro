import React, { useState } from "react";
import logo from "../../assets/images/logo_white.png";
import { Hidden, IconButton, Grow } from "@material-ui/core/";
import MenuIcon from "@material-ui/icons/Menu";
import "./NavigationBar.scss";
import { useLocation, useHistory } from "react-router-dom";

const NavigationBar = (props: { primary: boolean }) => {
  const [showMenu, setShowMenu] = useState(false);
  let location = useLocation();
  let history = useHistory();
  let navbarClassName;

  if (!props.primary) navbarClassName = "secondary";
  else navbarClassName = "primary";

  return (
    <div className={navbarClassName}>
      <div className="container">
        <nav className="navbar">
          <a className="navbar-logo" href="/">
            <img src={logo} alt="Logo" />
          </a>
          <Hidden smDown>
            <ul className="navbar-nav">
              <li className="navbar-item">
                <a href="/" className={location.pathname === "/" ? "navbar-link active" : "navbar-link"}>
                  Home
                </a>
              </li>
              <li className="navbar-item">
                <a href="/listings" className={location.pathname === "/listings" ? "navbar-link active" : "navbar-link"}>
                  Listings
                </a>
              </li>
              <li className="navbar-item">
                <a href="/about" className={location.pathname === "/about" ? "navbar-link active" : "navbar-link"}>
                  About Us
                </a>
              </li>
              <li className="navbar-item">
                <a href="/contact-us" className={location.pathname === "/contact-us" ? "navbar-link active" : "navbar-link"}>
                  Contact Us
                </a>
              </li>
            </ul>
            <a
              className="btn btn-outline-light navbar-button ml-2"
              href="tel:+6287824303455"
            >
              Call Us
            </a>
          </Hidden>
          <Hidden mdUp>
            <IconButton onClick={() => setShowMenu(!showMenu)}>
              <MenuIcon className="navbar-menu-button" />
            </IconButton>
          </Hidden>
        </nav>
        {showMenu === true ? (
          <Hidden mdUp>
            <Grow in>
              <div className="navbar-menu">
                <ul>
                  <li>
                    <a href="/">Home</a>
                  </li>
                  <li>
                    <a href="/listings">Listings</a>
                  </li>
                  <li>
                    <a href="/about">About Us</a>
                  </li>
                  <li>
                    <a href="/contact-us">Contact Us</a>
                  </li>
                </ul>
              </div>
            </Grow>
          </Hidden>
        ) : null}
      </div>
    </div>
  );
};

export default NavigationBar;
