import React from 'react';
import DefaultAvatar from '../../assets/images/default_avatar.png';
import {
  Card,
  CardActionArea,
  CardContent,
} from '@material-ui/core/';
import './ProfileCard.scss';

const ProfileCard = (props: { data: any, onClick: any }) => {
  const { data, onClick } = props;
	return (
      <Card className="profile-card">
        <CardActionArea onClick={onClick}>
          <CardContent>
            <div className="text-center">
              <img src={
              	data.profile_picture == null ? DefaultAvatar : data.profile_picture
              } className="avatar" alt={data.name}/>
              <h5 className="my-3 text-truncate">{ data.name == null ? data.username : data.name }</h5>
              <p className="text-truncate">{data.role}</p>
            </div>
          </CardContent>
        </CardActionArea>
      </Card>
	)
}

export default ProfileCard;