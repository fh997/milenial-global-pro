import React, { useState } from "react";
import NavigationBar from "../NavigationBar/NavigationBar";
import { useHistory } from "react-router-dom";
import qs from "query-string";
import "./Jumbotron.scss";
import SearchIcon from "@material-ui/icons/Search";
import { Card } from "@material-ui/core";

const Jumbotron = (props: { title?: string }) => {
  const [searchQuery, setSearchQuery] = useState("");
  const history = useHistory();
  const { title } = props;

  const onHandleSearch = (event: any) => {
    event.preventDefault();
    history.push({
      pathname: "/listings",
      search: qs.stringify({ query: searchQuery }),
    });
  };

  return (
    <header className="jumbotron">
      <div className="content">
        <NavigationBar primary />
        {!title ? (
          <div className="pb-5 container text-center">
            <h1 className="jumbotron-text mb-4">Cari property impian anda</h1>
            <p className="jumbotron-caption mb-3">
              Ketik lokasi berdasarkan daerah, provinsi, kota, atau kode pos.
            </p>
            <Card className="jumbotron-content">
              <form onSubmit={onHandleSearch}>
                <div className="form-group text-left">
                  <button type="button" className="btn btn-primary">
                    Beli
                  </button>
                  <button type="button" className="btn btn-clear">
                    Jual
                  </button>
                  <button type="button" className="btn btn-clear">
                    Sewa
                  </button>
                </div>
                <div className="form-group">
                  <div className="input-group">
                    <input
                      className="jumbotron-search-input form-control"
                      type="text"
                      placeholder="Cari properti berdasarkan daerah atau kota"
                      value={searchQuery}
                      onChange={(event) => setSearchQuery(event.target.value)}
                      required
                    />
                    <div className="input-group-append">
                      <button
                        type="submit"
                        className="btn btn-primary button-search"
                      >
                        <SearchIcon />
                      </button>
                    </div>
                  </div>
                </div>
              </form>
            </Card>
          </div>
        ) : (
          <div className="container">
            <h1 className="jumbotron-title">{title}</h1>
          </div>
        )}
      </div>
    </header>
  );
};

export default Jumbotron;
