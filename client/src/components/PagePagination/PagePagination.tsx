import React from 'react';
import Pagination from "@material-ui/lab/Pagination";
import './PagePagination.scss';

const PagePagination = (props: any) => {
  return (
    <div className="my-4 pagination">
      <Pagination
        color="primary"
        count={props.pageCount}
        onChange={props.onChange}
        showFirstButton
        showLastButton
      />
    </div>
  );
};

export default PagePagination;