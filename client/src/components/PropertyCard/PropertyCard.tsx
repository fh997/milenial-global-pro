import React from "react";
import {
  Card,
  CardActionArea,
  CardMedia,
  CardContent,
  Typography,
} from "@material-ui/core/";
import "./PropertyCard.scss";
import RoomOutlinedIcon from "@material-ui/icons/RoomOutlined";
import HotelOutlinedIcon from "@material-ui/icons/HotelOutlined";
import BathtubOutlinedIcon from "@material-ui/icons/BathtubOutlined";
import HouseOutlinedIcon from "@material-ui/icons/HouseOutlined";
import NumberFormat from "react-number-format";

const PropertyCard = (props: { data: any; href?: string; onClick: any }) => {
  const { data, href, onClick } = props;
  return (
    <div className="property-card">
      <Card>
        <CardActionArea onClick={onClick}>
          <CardMedia
            component="img"
            alt="house"
            height="200"
            image={
              data.url_path ??
              "https://i.ytimg.com/vi/xYkz0Ueg0L4/maxresdefault.jpg"
            }
          />
          <CardContent>
            <div className="property-detail mb-2">
              <RoomOutlinedIcon className="detail-icon" />
              <p className="detail-text">
                {data.address}, {data.city}
              </p>
            </div>
            <h5 className="text-truncate mb-2">{data.title}</h5>
            <p className="text-truncate">
              <NumberFormat
                value={data.price}
                displayType={"text"}
                thousandSeparator={true}
                prefix={"Rp. "}
              />
            </p>
            <div className="property-detail mt-2">
              <div className="property-detail mr-3">
                <HotelOutlinedIcon className="detail-icon" />
                <p className="detail-text">
                  {data.bedrooms > 5 ? "5+" : data.bedrooms}
                </p>
              </div>
              <div className="property-detail mr-3">
                <BathtubOutlinedIcon className="detail-icon" />
                <p className="detail-text">
                  {data.bathrooms > 5 ? "5+" : data.bathrooms}
                </p>
              </div>
            </div>
            <div className="property-detail mt-2">
              <HouseOutlinedIcon className="detail-icon" />
              <p className="detail-text">
                LT: {data.land_area} m<sup>2</sup> / LB: {data.floor_area} m
                <sup>2</sup>
              </p>
            </div>
          </CardContent>
        </CardActionArea>
      </Card>
    </div>
  );
};

export default PropertyCard;
