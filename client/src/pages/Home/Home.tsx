/*
  Milenial Global Pro
  ===================
  Home Page
  Created by Felix Husen
*/

import React, { Component } from 'react';
import Jumbotron from '../../components/Jumbotron/Jumbotron';
import SuperBar from '../../components/SuperBar/SuperBar';
import ServiceBar from '../../components/ServiceBar/ServiceBar';
import ContactBar from '../../components/ContactBar/ContactBar';
import Footer from '../../components/Footer/Footer';
import LatestProperties from '../../components/LatestProperties/LatestProperties';
import { RouteComponentProps } from 'react-router';
import { GetPropertyOutput } from '../../services/property/dto/getPropertyOutput';
import propertyService from "../../services/property/propertyService";
import { GetAllPropertyInput } from '../../services/property/dto/getAllPropertyInput';

interface IHomeProps extends RouteComponentProps<any> {}

interface IHomeStates {
  properties: GetPropertyOutput[];
}

class Home extends Component<IHomeProps, IHomeStates> {
  constructor(props: IHomeProps) {
    super(props);
    this.state = {
      properties: []
    }
  }

  public componentDidMount() {
    // Setting up the page title
    document.title = 'Milenial Global Pro';
    // Requesting all property listing
    const input: GetAllPropertyInput = {
      sorting: "properties.id desc",
      maxResultCount: 3
    }
    propertyService.getAll(input).then(res => this.setState({ properties: res.data.data }))
  }
  
  render() {
    return (
      <div>
        <Jumbotron />
        <SuperBar />
        <ServiceBar />
        <LatestProperties data={this.state.properties} />
        <ContactBar />
        <Footer />
      </div>
    );
  }
}

export default Home;
