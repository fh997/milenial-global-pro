import React, { Component } from 'react';
import {
  Fab,
  Typography,
  CircularProgress,
  IconButton
} from '@material-ui/core/';
import { withSnackbar, WithSnackbarProps } from 'notistack';
import DeleteIcon from '@material-ui/icons/Delete';
import SaveIcon from '@material-ui/icons/Save';
import NavigationBar from '../../../components/Admin/NavigationBar/NavigationBar';
import '../../../style.scss';
import { RouteComponentProps } from 'react-router';

interface IManageUserProps extends RouteComponentProps<any>, WithSnackbarProps {}

interface IManageUserStates {
  user: {
    fullname: string,
    username: string,
    password: string,
    role_id: number
  };
  roles: any[];
  isEditEmployee: boolean;
  title: string;
  isLoading: boolean;
}

class ManageUser extends Component<IManageUserProps, IManageUserStates> {

  constructor(props: IManageUserProps) {
    super(props);
    this.state = {
      user: {
        fullname: '',
        username: '',
        password: '',
        role_id: 1,
      },
      roles: [],
      isEditEmployee: false,
      title: 'Add New User',
      isLoading: true,
    }
  }

  componentDidMount() {
    // const { params } = this.props.match
    // api.getRoles().then(res => this.setState({ roles: res.data }))

    // /* Check if it is adding a new user or updating an existing user */
    // if (params.id != null) {
    //   api.getUser(params.id)
    //   .then(result => {
    //     this.setState({ user: result.data[0], title: 'Manage User', isLoading: false, isEditEmployee: true });
    //     document.title = 'Manage User | Milenial Global Pro';
    //   });
    // } else {
    //   document.title = 'Add New User | Milenial Global Pro';
    //   this.setState({ isLoading: false });
    // }
  }

  deleteUserHandler = () => {
    // Sending delete user request
    // api.deleteUser(this.state.user.id)
    // .then(() => {
    //   this.props.enqueueSnackbar('User has been deleted.', { variant: 'success' })
    // }).catch(() => {
    //   this.props.enqueueSnackbar('Cannot delete user.', { variant: 'error' })
    // }).finally(() => {
    //   this.props.history.push('/admin/manage-users');
    // });
  }

  actionComponents = (
    <IconButton color='inherit' onClick={this.deleteUserHandler}>
      <DeleteIcon />
    </IconButton>
  )

  updateInputValue = (event: any) => {
    this.setState({ user: { ...this.state.user, [event.target.id]: event.target.value }});
  }

  saveChangesHandler = () => {
    this.setState({ isLoading: true });

    // Check if it is editing or adding
    // if (this.state.isEditEmployee) {
    //   api.updateUser(this.state.user).then(res => {
    //     this.props.enqueueSnackbar('Changes saved.', { variant: 'success' })
    //   }).catch(res => {
    //     this.props.enqueueSnackbar('Cannot save changes.', { variant: 'error' })
    //   }).finally(() => {
    //     this.setState({ isLoading: false });
    //   })
    // } else {
    //   api.addUser(this.state.user).then(res => {
    //     this.props.enqueueSnackbar('Changes saved.', { variant: 'success' })
    //     this.props.history.push('/admin/manage-users');
    //   }).catch(res => {
    //     this.props.enqueueSnackbar('Cannot save changes.', { variant: 'error' })
    //   }).finally(() => {
    //     this.setState({ isLoading: false });
    //   })
    // }
    
  }

  render() {
    const { user, roles, title, isLoading } = this.state;
    return (
      <div>
        <NavigationBar title={title} actionComponents={this.actionComponents}/>
        <div className='pa3'>
          { 
            isLoading === true ? (
              <div className='flex justify-center items-center pa5'>
                <CircularProgress />
              </div>
            ) : (
              <div>
                <Typography paragraph variant='h6' color='primary'>Personal Details</Typography>
                <Typography paragraph variant='body1'>Full Name</Typography>
                <input className='w-100 mb3' id='fullname' type='text' placeholder='Full Name' value={user.fullname} onChange={this.updateInputValue}/>
                <Typography paragraph variant='body1'>Username</Typography>
                <input className='w-100 mb3' id='username' type='text' placeholder='Username' value={user.username} onChange={this.updateInputValue}/>
                <Typography paragraph variant='body1'>Password</Typography>
                <input className='w-100 mb3' id='password' type='password' placeholder='Password' value={user.password} onChange={this.updateInputValue}/>
                <Typography paragraph variant='body1'>Role</Typography>
                <select id='role_id' value={user.role_id} onChange={this.updateInputValue}>
                  {
                    roles.map(role => {
                      return (
                        <option value={role.id} key={role.id}>{role.name}</option>
                      )
                    })
                  }
                </select>
              </div>
            )
          }
        </div>
        <div className='fab'>
          <Fab color="primary" aria-label="add" onClick={this.saveChangesHandler}>
            <SaveIcon />
          </Fab>
        </div>
      </div>
    );
  }
}

export default withSnackbar(ManageUser);
