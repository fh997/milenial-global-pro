import React from 'react';
import logo from '../../assets/images/logo_white.png';
import {
	Grid,
} from '@material-ui/core/';
import RoomIcon from '@material-ui/icons/Room';
import PhoneIcon from '@material-ui/icons/Phone';
import EmailIcon from '@material-ui/icons/Email';
import '../../style.scss';

const Footer = () => {
	const year = new Date().getFullYear();
	return (
		<footer className='footer'>
			<div className='container p-5'>
				<Grid container spacing={5}>
					<Grid item sm={12} md={4}>
						<a className='navbar-logo' href='/'><img src={logo} alt='Logo' /></a>
					</Grid>
					<Grid item sm={12} md={4}>
						<h5>Important Links</h5>
						<ul className='p-0 py-3'>
							<li><a href='/'>Home</a></li>
							<li><a href='/listings'>Listings</a></li>
							<li><a href='/about'>About Us</a></li>
							<li><a href='/contact-us'>Contact Us</a></li>
						</ul>
					</Grid>
					<Grid item sm={12} md={4}>
						<h5>Contact Information</h5>
						<ul className='p-0 py-3'>
							<li className='pb3'>
								<a href='http://maps.google.com/?q=Ruko Mekar Utama'>
									<RoomIcon className='contact-icon'/>
									<span className='ml2'>Ruko Mekar Utama 1 No. 12</span><br/>
									<span className='ml4'>Komp Mekar Wangi</span><br/>
									<span className='ml4'>Bandung, 40237</span>
								</a>
							</li>
							<li className='pb-3'><a href='tel:+6282120699973'><PhoneIcon className='contact-icon'/><span className='ml2'>0821 2069 9973 / 0815 606 5512</span></a></li>
							<li className='pb-3'><a href='mailto:millenials.propertindo@gmail.com'><EmailIcon className='contact-icon'/><span className='ml2'>millenials.propertindo@gmail.com</span></a></li>
						</ul>
					</Grid>
				</Grid>
				<span>© {year} Milenial Global Pro. Designed by Felix Husen.</span>
			</div>
        </footer>
	)
}

export default Footer;