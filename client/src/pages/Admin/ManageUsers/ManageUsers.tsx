/*
  Milenial Global Pro
  ===================
  Admin Manage User Page
  Created by Felix Husen
*/

import React, { Component } from 'react';
import { Grid, Fab } from '@material-ui/core/';
import { withSnackbar, WithSnackbarProps } from 'notistack';
import AddIcon from '@material-ui/icons/Add';
import ProfileCard from '../../../components/ProfileCard/ProfileCard';
import NavigationBar from '../../../components/Admin/NavigationBar/NavigationBar';
import '../../../style.scss';
import { RouteComponentProps } from 'react-router';

interface IManageUsersProps extends RouteComponentProps<any>, WithSnackbarProps {}

interface IManageUsersStates {
  users: any[]
}

class ManageUsers extends Component<IManageUsersProps, IManageUsersStates> {

  constructor(props: IManageUsersProps) {
    super(props);
    this.state = {
      users: []
    }
  }

  componentDidMount() {
    // api.getUsers().then(res => this.setState({ users: res.data }))
  }

  render() {
    const { users } = this.state;
    return (
      <div className='manage-employees'>
        <NavigationBar title={'Manage Users'}/>
        <div className='pa3'>
          <Grid container spacing={3}>
            {
              users.map((item, i) => {
                return (
                  <Grid item xs={12} md={4} lg={3} key={i}>
                    <ProfileCard data={item} onClick={() => this.props.history.push(`/admin/manage-user/${item.id}`)}/>
                  </Grid>
                )
              })
            }
          </Grid>
        </div>
        <div className='fab'>
          <Fab color="primary" aria-label="add" href='/admin/add-user'>
            <AddIcon />
          </Fab>
        </div>
      </div>
    );
  }
}

export default withSnackbar(ManageUsers);
