import React, { Component } from "react";
import { Grid, Fab } from "@material-ui/core/";
import { RouteComponentProps } from "react-router-dom";
import ProfileCard from "../../../components/ProfileCard/ProfileCard";
import NavigationBar from "../../../components/Admin/NavigationBar/NavigationBar";
import AddIcon from "@material-ui/icons/Add";
import employeeService from "../../../services/employee/employeeService";
import { GetAllEmployeeInput } from "../../../services/employee/dto/getAllEmployeeInput";
import { GetEmployeeOutput } from "../../../services/employee/dto/getEmployeeOutput";

interface IManageEmployeesProps extends RouteComponentProps<any> {}

interface IManageEmployeesState {
  employees: GetEmployeeOutput[];
  filterText: string;
  skipCount: number | null;
  maxResultCount: number | null;
}

export default class ManageEmployees extends Component<
  IManageEmployeesProps,
  IManageEmployeesState
> {
  constructor(props: IManageEmployeesProps) {
    super(props);
    this.state = {
      employees: [],
      filterText: "",
      skipCount: null,
      maxResultCount: null
    };
  }

  public componentDidMount = () => {
    this.getEmployees();
  }

  public getEmployees = () => {
    const input: GetAllEmployeeInput = {
      filter: this.state.filterText,
      skipCount: this.state.skipCount,
      maxResultCount: this.state.maxResultCount
    };
    employeeService.getAll(input).then(res => {
      const result = res.data.data;
      this.setState({ employees: result });
    });
  }

  public showDetails = (employee: any) => {
    this.props.history.push(`/admin/manage-employee/${employee.id}`)
  };

  public addEmployee = () => {
    this.props.history.push("/admin/add-employee");
  };

  public render() {
    const { employees } = this.state;
    return (
      <div className="manage-employees">
        <NavigationBar title={"Manage Employees"} />
        <div className="p-3">
          <Grid container spacing={3}>
            {employees.map((item, i) => {
              return (
                <Grid item xs={12} md={4} lg={3} key={i}>
                  <ProfileCard
                    data={item}
                    onClick={() => this.showDetails(item)}
                  />
                </Grid>
              );
            })}
          </Grid>
        </div>
        <div className="fab">
          <Fab color="primary" aria-label="add" onClick={this.addEmployee}>
            <AddIcon />
          </Fab>
        </div>
      </div>
    );
  }
}
