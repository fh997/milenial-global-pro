import React, {useState} from "react";
import {useLocation, useHistory} from "react-router";
import "./SearchSidebar.scss";
import {Grid, Slider} from "@material-ui/core/";
import {useSnackbar} from "notistack";
import qs from "query-string";

export interface ISearchParams {
  query?: string;
  minBedrooms?: number;
  minBathrooms?: number;
  province?: string;
  minPrice?: number;
  maxPrice?: number;
}

const SearchSidebar = () => {
  const {enqueueSnackbar} = useSnackbar();

  const location = useLocation();
  const history = useHistory();

  const currentParams = qs.parse(location.search);

  const [searchParams, setSearchParams] = useState<ISearchParams>({
    query: "",
    minBedrooms: 0,
    minBathrooms: 0,
    minPrice: 0,
    maxPrice: 0,
    province: "",
    ...currentParams,
  });

  if (searchParams.minBedrooms)
    searchParams.minBedrooms = Number(searchParams.minBedrooms);
  if (searchParams.minBathrooms)
    searchParams.minBathrooms = Number(searchParams.minBathrooms);

  const roomOptions = [
    {
      value: 0,
      label: "Any",
    },
    {
      value: 1,
      label: "1+",
    },
    {
      value: 2,
      label: "2+",
    },
    {
      value: 3,
      label: "3+",
    },
    {
      value: 4,
      label: "4+",
    },
    {
      value: 5,
      label: "5+",
    },
  ];

  const updateInputValue = (event: any) => {
    setSearchParams({...searchParams, [event.target.name]: event.target.value});
  };

  const updateSliderValue = (event: any, value: any) => {
    setSearchParams({...searchParams, [event.target.id]: value});
  };

  // const handleSearch = (event: any) => {
  //   event.preventDefault();
  //   history.push({ pathname: '/listings', search: event.})
  // };

  return (
    <form className="search-sidebar" action='/listings'>
      <div>
        <p className="pb-2">Search Property</p>
        <input
          type="text"
          name="query"
          placeholder="Search..."
          onChange={updateInputValue}
          value={searchParams.query}
        />
      </div>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <div>
            <p className="my-2">Bedrooms</p>
            <div className="mx-4">
              <Slider
                name="minBedrooms"
                id="minBedrooms"
                min={0}
                max={5}
                defaultValue={0}
                aria-labelledby="discrete-slider-restrict"
                step={null}
                valueLabelDisplay="off"
                marks={roomOptions}
                onChange={updateSliderValue}
                value={searchParams.minBedrooms}
              />
            </div>
          </div>
        </Grid>
        <Grid item xs={12}>
          <div>
            <p className="my-2">Bathrooms</p>
            <div className="mx-4">
              <Slider
                name="minBathrooms"
                id="minBathrooms"
                defaultValue={0}
                min={0}
                max={5}
                aria-labelledby="discrete-slider-restrict"
                step={null}
                valueLabelDisplay="off"
                marks={roomOptions}
                onChange={updateSliderValue}
                value={searchParams.minBathrooms}
              />
            </div>
          </div>
        </Grid>
        <Grid item xs={12}>
          <div>
            <p className="py-2">Location</p>
            <input
              type="text"
              name="province"
              placeholder="Property Location"
              onChange={updateInputValue}
              value={searchParams.province}
            />
          </div>
        </Grid>
        <Grid item xs={12} sm={12}>
          <div>
            <p className="pv2">Minimum Price</p>
            <input
              type="number"
              name="minPrice"
              min={0}
              placeholder="Minimum Price"
              onChange={updateInputValue}
              value={searchParams.minPrice}
            />
          </div>
        </Grid>
        {/* <Grid item xs={12} sm={6}>
          <div>
            <p className="pv2">Maximum Price</p>
            <input
              type="number"
              name="maxPrice"
              min={0}
              placeholder="Maximum Price"
              onChange={updateInputValue}
              value={searchParams.maxPrice}
            />
          </div>
        </Grid> */}
      </Grid>

      <button className="btn btn-primary w-100 mt-3" type="submit">
        Search
      </button>
    </form>
  );
};

export default SearchSidebar;
