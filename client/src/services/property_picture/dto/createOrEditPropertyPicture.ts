export interface CreateOrEditPropertyPicture {
  id: number;
  url_path: string;
  filename: string;
  uploaded_on: string;
  property_id: number;
}
