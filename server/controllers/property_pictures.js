const asyncHandler = require("../middleware/async");
const db = require("../config/db");
const ErrorResponse = require("../utils/errorResponse");
const path = require("path");
const fs = require("fs");
const cloudinary = require('cloudinary').v2;

/**
 * @swagger
 * path:
 *  /api/v1/property-picture/GetAll:
 *    get:
 *      summary: Get all property pictures
 *      tags: [PropertyPictures]
 *      produces:
 *          - application/json
 *      parameters:
 *        - name: propertyId
 *          in: query
 *          description: Property id
 *          required: false
 *          type: string
 *      responses:
 *        "200":
 *          description: Success
 */

exports.getAll = asyncHandler(async (req, res, next) => {
  db
    .select()
    .table("property_pictures")
    .modify(db =>
      req.query.propertyId != null ?
      db.where("property_id", "=", req.query.propertyId) :
      true
    )
    .then(result => {
      return res.status(200).json(result);
    });
});

/**
 * @swagger
 * path:
 *  /api/v1/property-picture/GetPropertyPictureForView:
 *    get:
 *      summary: Get a property picture with an id given
 *      tags: [PropertyPictures]
 *      produces:
 *          - application/json
 *      parameters:
 *        - name: id
 *          in: query
 *          description: Property picture id
 *          required: true
 *          type: string
 *      responses:
 *        "200":
 *          description: Success
 */

exports.getPropertyPictureForView = asyncHandler(async (req, res, next) => {
  db
    .select()
    .table("property_pictures")
    .where("id", "=", req.params.id)
    .then(result => {
      return res.status(200).json(result);
    });
});

/**
 * @swagger
 * path:
 *  /api/v1/property-picture/Create:
 *    post:
 *      summary: Insert a property picture
 *      tags: [PropertyPictures]
 *      produces:
 *          - application/json
 *      consumes:
 *        - multipart/form-data
 *      parameters:
 *        - name: propertyId
 *          in: query
 *          description: Property id
 *          required: true
 *          type: integer
 *        - name: file
 *          in: body
 *          description: Picture file
 *          required: true
 *          type: file
 *      responses:
 *        "200":
 *          description: Success
 */

exports.create = asyncHandler(async (req, res, next) => {
  const file = req.files.file;

  if (!req.files) {
    return next(new ErrorResponse(`Please upload a file`, 400));
  }

  // Make sure the image is a photo
  if (!file.mimetype.startsWith('image')) {
    return next(new ErrorResponse(`Please upload an image file`, 400));
  }

  // Check filesize
  if (file.size > process.env.MAX_FILE_UPLOAD) {
    return next(
      new ErrorResponse(
        `Please upload an image less than ${process.env.MAX_FILE_UPLOAD}`,
        400
      )
    );
  }

  const fileName = `property_${Date.now()}`;

  file.name = fileName + path.parse(file.name).ext;

  file.mv(`${process.env.FILE_UPLOAD_PATH}/${file.name}`, async err => {
    const fileUrl = `${process.env.FILE_UPLOAD_URL}${file.name}`;

    const propertyPicture = {
      url_path: fileUrl,
      filename: file.name,
      uploaded_on: new Date(),
      property_id: Number(req.query.propertyId)
    };

    if (err) {
      console.error(err);
      return next(new ErrorResponse(`Problem with file upload`, 500));
    }

    cloudinary.uploader.upload(`${process.env.FILE_UPLOAD_PATH}/${file.name}`, {
        public_id: fileName
      },
      function (error, result) {
        if (!error) {
          propertyPicture.url_path = result.url;
          db("property_pictures")
            .insert(propertyPicture).then(() => {
              return res.status(200).json({
                success: true,
                data: propertyPicture
              });
            })
            .catch(err => {
              return res.status(500).json({
                success: false,
                error: err.toString()
              });
            });
        }
      }
    );
  });
});

exports.uploadImage = async (file, fileName) => {
  return new Promise((resolve, reject) => {
    cloudinary.uploader.upload(file, {
        public_id: fileName
      },
      function (error, result) {
        if (!error) {
          return resolve(result.url);
        } else {
          return reject(null);
        }
      }
    );
  });
}

/**
 * @swagger
 * path:
 *  /api/v1/property-picture/Delete:
 *    delete:
 *      summary: Delete a property picture
 *      tags: [PropertyPictures]
 *      produces:
 *          - application/json
 *      parameters:
 *        - name: id
 *          in: query
 *          description: PropertyPicture id
 *          required: true
 *          type: integer
 *      responses:
 *        "200":
 *          description: Success
 */

exports.deletePropertyPicture = asyncHandler(async (req, res, next) => {
  db
    .select()
    .table("property_pictures")
    .where("id", "=", req.query.id)
    .then(result => {
      if (result.length == 0) {
        return res.status(200).json({
          success: false
        });
      }

      const propertyPicture = result[0];

      const filePath =
        process.env.FILE_UPLOAD_PATH +
        String(propertyPicture.url_path).replace(
          process.env.FILE_UPLOAD_URL,
          ""
        );

      const public_id = path.parse(propertyPicture.filename).name;

      cloudinary.uploader.destroy(public_id, {}, () => {});
      fs.unlink(filePath, () => {});
    });

  db("property_pictures")
    .where("id", "=", req.query.id)
    .del()
    .then(() => {
      return res.status(200).json({
        success: true
      });
    })
    .catch(err => {
      return res.status(500).json({
        success: false,
        error: err.toString()
      });
    });
});