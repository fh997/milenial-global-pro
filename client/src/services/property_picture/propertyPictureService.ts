import { GetAllPropertyPictureInput } from "./dto/getAllPropertyInput";
import http from "../httpService";
import authenticationService from "../authentication/authenticationService";

class PropertyPictureService {
  public async getAll(input: GetAllPropertyPictureInput) {
    let result = await http.get("property-picture/GetAll", { params: input });
    return result;
  }

  public async getPropertyPictureForView(propertyPictureId: number) {
    let result = await http.get("property-picture/GetPropertyForView", {
      params: { id: propertyPictureId }
    });
    return result;
  }

  public async create(propertyId: number, pictures: FormData) {
    let result = await http.post("property-picture/Create", pictures, {
      headers: authenticationService.authHeader(),
      params: { propertyId: propertyId }
    });
    return result;
  }

  public async delete(propertyId: number) {
    let result = await http.delete("property-picture/Delete", {
      headers: authenticationService.authHeader(),
      params: { id: propertyId }
    });
    return result;
  }
}

export default new PropertyPictureService();
