import React, {Component} from "react";
import { Grid, CircularProgress } from "@material-ui/core/";
import Jumbotron from "../../components/Jumbotron/Jumbotron";
import ContactBar from "../../components/ContactBar/ContactBar";
import SearchSidebar from "../../components/SearchSidebar/SearchSidebar";
import Footer from "../../components/Footer/Footer";
import PropertyImagesCarousel from "../../components/PropertyImagesCarousel/PropertyImagesCarousel";
import RoomOutlinedIcon from "@material-ui/icons/RoomOutlined";
import HotelOutlinedIcon from "@material-ui/icons/HotelOutlined";
import BathtubOutlinedIcon from "@material-ui/icons/BathtubOutlined";
import HouseOutlinedIcon from "@material-ui/icons/HouseOutlined";
// import DefaultAvatar from "../../assets/images/default_avatar.png";
import NumberFormat from "react-number-format";
import ReactHtmlParser from "react-html-parser";
import "./Listing.scss";
import {GetPropertyOutput} from "../../services/property/dto/getPropertyOutput";
import {GetPropertyPictureOutput} from "../../services/property_picture/dto/getPropertyPictureOutput";
import {RouteComponentProps} from "react-router";
import propertyService from "../../services/property/propertyService";
import propertyPictureService from "../../services/property_picture/propertyPictureService";

interface IListingProps extends RouteComponentProps<any> {}

interface IListingStates {
  property: GetPropertyOutput;
  propertyPictures: GetPropertyPictureOutput[];
  loading: boolean;
}

class Listing extends Component<IListingProps, IListingStates> {
  constructor(props: IListingProps) {
    super(props);
    this.state = {
      property: {
        id: 0,
        title: "",
        short_description: "",
        description: "",
        bathrooms: 0,
        bedrooms: 0,
        garage: 0,
        land_area: 0,
        floor_area: 0,
        price: 0,
        address: "",
        city: "",
        postcode: 0,
        province: "",
      },
      propertyPictures: [],
      loading: false
    };
  }

  public async componentDidMount() {
    document.title = "Listing | Milenial Global Pro";
    this.getProperty();
    this.getPropertyPictures();
  }

  public getProperty = () => {
    this.setState({ loading: true });
    const {params} = this.props.match;
    propertyService.getPropertyForView(params.id).then((res) => {
      this.setState({property: res.data});
      document.title = `${res.data.title} | Milenial Global Pro`;
    });
  };

  public getPropertyPictures = () => {
    const {params} = this.props.match;
    propertyPictureService.getAll({propertyId: params.id}).then((res) => {
      this.setState({propertyPictures: res.data, loading: false});
    });
  };

  render() {
    const {property, propertyPictures, loading} = this.state;

    return (
      <div className="listing-page">
        <Jumbotron title={property.title} />
        <div className="container py-5">
          <Grid container spacing={5}>
            <Grid item xs={12} sm={5} md={4}>
              <div className="pv4">
                <SearchSidebar />
              </div>
            </Grid>
            <Grid item xs={12} sm={7} md={8}>
              {loading ? (
                <div className="vh-100 w-100 flex items-center justify-center">
                  <CircularProgress />
                </div>
              ) : (
                <div className="py-4">
                  <PropertyImagesCarousel images={propertyPictures} />
                  <h5 className="py-2">
                    <RoomOutlinedIcon className="detail-icon" />
                    {property.city}
                  </h5>
                  <hr />
                  <h5 className="py-2">
                    <HotelOutlinedIcon className="detail-icon" />
                    Kamar Tidur: {property.bedrooms}
                  </h5>
                  <h5 className="py-2">
                    <BathtubOutlinedIcon className="detail-icon" />
                    Kamar Mandi: {property.bathrooms}
                  </h5>
                  <h5 className="py-2">
                    <HouseOutlinedIcon className="detail-icon" />
                    LT: {property.land_area} m<sup>2</sup> / LB:{" "}
                    {property.floor_area} m<sup>2</sup>
                  </h5>
                  <h3 className="py-3">
                    <NumberFormat
                      value={property.price}
                      displayType={"text"}
                      thousandSeparator={true}
                      prefix={"Rp. "}
                    />
                  </h3>
                  <hr />
                  <div className="property-description py-3">
                    {ReactHtmlParser(property.description)}
                  </div>
                  {/* <h2>Contact Us</h2> */}

                  {/* <div className='agent-information'>
                  <img src={
                    property.agent_profile_picture == null ? DefaultAvatar : property.agent_profile_picture
                  } className="br-100 h3 w3 dib pa1" alt={property.agent_name}/>
                  <div className='agent-contact'>
                    <h3>{property.agent_name}</h3>
                    <p>{property.agent_role_name}</p>
                    <p>{property.agent_phone_number}</p>
                  </div>
                </div> */}
                </div>
              )}
            </Grid>
          </Grid>
        </div>
        <ContactBar />
        <Footer />
      </div>
    );
  }
}

export default Listing;
