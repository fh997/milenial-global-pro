import {GetAllEmployeeInput} from "./dto/getAllEmployeeInput";
import {CreateOrEditEmployee} from "./dto/createOrEditEmployee";
import http from "../httpService";
import authenticationService from "../authentication/authenticationService";

class EmployeeService {
  public async getAll(input?: GetAllEmployeeInput) {
    let result = await http.get("employee/GetAll", {params: input});
    return result;
  }

  public async getEmployeeForView(employeeId: number) {
    let result = await http.get("employee/GetEmployeeForView", {
      params: {employeeId: employeeId}
    });
    return result;
  }

  public async uploadProfilePicture(employeeId: number, profilePicture: FormData) {
    let result = await http.post("employee/UploadProfilePicture", profilePicture, {
      headers: authenticationService.authHeader(),
      params: { employeeId: employeeId }
    });
    return result;
  }

  public async deleteProfilePicture(employeeId: number) {
    let result = await http.delete("employee/DeleteProfilePicture", {
      headers: authenticationService.authHeader(),
      params: {employeeId: employeeId}
    });
    return result;
  }

  public async createOrEdit(createOrEditEmployeeDto: CreateOrEditEmployee) {
    let result = await http.post("employee/CreateOrEdit", createOrEditEmployeeDto, {
      headers: authenticationService.authHeader()
    });
    return result;
  }

  public async delete(employeeId: number) {
    let result = await http.delete("employee/Delete", {
      headers: authenticationService.authHeader(),
      params: {id: employeeId}
    });
    return result;
  }
}

export default new EmployeeService();
