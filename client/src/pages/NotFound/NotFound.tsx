/*
  Milenial Global Pro
  ===================
  Not Found Page
  Created by Felix Husen
*/

import React, { Component } from 'react';
import JumbotronTitle from '../../components/JumbotronTitle/JumbotronTitle';
import Footer from '../../components/Footer/Footer';

class NotFound extends Component {
  
  render() {
    return (
      <div className="listing-page">
        <JumbotronTitle title={""}/>
        <div className='tc pa4'>
          <h1 className="f1 f-headline-l code mb3 fw9 dib tracked-tight">404</h1>
          <p>Sorry, the page that you are looking for isn't found</p>
        </div>
        <div className='tc pa4'>
          <h3>Are you looking for one of these?</h3>
          <a className='db mv4' href='/'>Home</a>
          <a className='db mv4' href='/listings'>Listings</a>
          <a className='db mv4' href='/about'>About Us</a>
          <a className='db mv4' href='/contact-us'>Contact Us</a>
        </div>
        <Footer />
      </div>
    );
  }
}

export default NotFound;
