export interface GetPropertyOutput {
  id: number;
  title: string;
  short_description: string;
  description: string;
  bathrooms: number;
  bedrooms: number;
  garage: number;
  land_area: number;
  floor_area: number;
  price: number;
  address: string;
  city: string;
  postcode: number;
  province: string;
  url_path?: string;
}
