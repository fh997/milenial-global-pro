export interface GetUserForViewDto {
  id: number;
  username: string;
  password: string;
  profile_picture: string;
  join_date: Date;
  role: number;
  token: string;
}
