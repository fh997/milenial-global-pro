const asyncHandler = require("../middleware/async");
const db = require("../config/db");
const path = require("path");
const cloudinary = require('cloudinary').v2;
const request = require('request');
const propertyPicture = require('./property_pictures');

/**
 * @swagger
 * path:
 *  /api/v1/property/GetAll:
 *    get:
 *      summary: Get all properties
 *      tags: [Properties]
 *      produces:
 *          - application/json
 *      parameters:
 *        - name: filter
 *          in: query
 *          description: Filter for search query.
 *          required: false
 *          type: string
 *        - name: minBathrooms
 *          in: query
 *          description: Minimum bathroom filter
 *          required: false
 *          type: integer
 *        - name: maxBathrooms
 *          in: query
 *          description: Maximum bathroom filter
 *          required: false
 *          type: integer
 *        - name: minBedrooms
 *          in: query
 *          description: Minimum Bedrooms filter
 *          required: false
 *          type: integer
 *        - name: maxBedrooms
 *          in: query
 *          description: Maximum Bedrooms filter
 *          required: false
 *          type: integer
 *        - name: minGarages
 *          in: query
 *          description: Minimum Garages filter
 *          required: false
 *          type: integer
 *        - name: maxGarages
 *          in: query
 *          description: Maximum Garages filter
 *          required: false
 *          type: integer
 *        - name: minLandArea
 *          in: query
 *          description: Minimum LandArea filter
 *          required: false
 *          type: integer
 *        - name: maxLandArea
 *          in: query
 *          description: Maximum LandArea filter
 *          required: false
 *          type: integer
 *        - name: minFloorArea
 *          in: query
 *          description: Minimum FloorArea filter
 *          required: false
 *          type: integer
 *        - name: maxFloorArea
 *          in: query
 *          description: Maximum FloorArea filter
 *          required: false
 *          type: integer
 *        - name: minPrice
 *          in: query
 *          description: Minimum Price filter
 *          required: false
 *          type: integer
 *        - name: maxPrice
 *          in: query
 *          description: Maximum Price filter
 *          required: false
 *          type: integer
 *        - name: postCode
 *          in: query
 *          description: Postcode filter
 *          required: false
 *          type: integer
 *        - name: city
 *          in: query
 *          description: City filter
 *          required: false
 *          type: string
 *        - name: province
 *          in: query
 *          description: Province filter
 *          required: false
 *          type: string
 *        - name: maxResultCount
 *          in: query
 *          description: Maximum result count for paging.
 *          required: false
 *          type: integer
 *        - name: skipCount
 *          in: query
 *          description: Skip count for paging.
 *          required: false
 *          type: integer
 *        - name: sorting
 *          in: query
 *          description: Sorting method.
 *          required: false
 *          type: string
 *      responses:
 *        "200":
 *          description: Returns all properties on the system
 */

exports.getAll = asyncHandler(async (req, res, next) => {
  let {
    filter,
    minBathrooms,
    maxBathrooms,
    minBedrooms,
    maxBedrooms,
    minGarages,
    maxGarages,
    minLandArea,
    maxLandArea,
    minFloorArea,
    maxFloorArea,
    minPrice,
    maxPrice,
    city,
    postCode,
    province,
    sorting,
    skipCount,
    maxResultCount
  } = req.query;
  const properties = db
    .select()
    .table("properties")
    .leftOuterJoin(
      db.raw(`(select distinct on (property_id) url_path, property_id from property_pictures) as pp`),
      'pp.property_id',
      'properties.id')
    .modify(db =>
      filter != null ?
      db.whereRaw(
        "LOWER(title) LIKE '%' || LOWER(?) || '%' ",
        filter.toLowerCase()
      ) :
      true
    )
    .modify(db =>
      city != null ?
      db.whereRaw(
        "LOWER(city) LIKE '%' || LOWER(?) || '%' ",
        city.toLowerCase()
      ) :
      true
    )
    .modify(db =>
      province != null ?
      db.whereRaw(
        "LOWER(province) LIKE '%' || LOWER(?) || '%' ",
        province.toLowerCase()
      ) :
      true
    )
    .modify(db =>
      postCode != null ? db.where("postcode", "=", postCode) : true
    )
    .modify(db =>
      minBathrooms != null ?
      db.where("bathrooms", ">=", minBathrooms) :
      true
    )
    .modify(db =>
      maxBathrooms != null ?
      db.where("bathrooms", "<=", maxBathrooms) :
      true
    )
    .modify(db =>
      minBedrooms != null ?
      db.where("bedrooms", ">=", minBedrooms) :
      true
    )
    .modify(db =>
      maxBedrooms != null ?
      db.where("bedrooms", "<=", maxBedrooms) :
      true
    )
    .modify(db =>
      minGarages != null && maxGarages != null ?
      db.where("garage", "<=", maxGarages).where("garage", ">=", minGarages) :
      true
    )
    .modify(db =>
      minLandArea != null && maxLandArea != null ?
      db
      .where("land_area", "<=", maxLandArea)
      .where("land_area", ">=", minLandArea) :
      true
    )
    .modify(db =>
      minFloorArea != null && maxFloorArea != null ?
      db
      .where("floor_area", "<=", maxFloorArea)
      .where("floor_area", ">=", minFloorArea) :
      true
    )
    .modify(db =>
      minPrice != null ?
      db.where("price", ">=", minPrice) :
      true
    )
    .modify(db =>
      maxPrice != null ?
      db.where("price", "<=", maxPrice) :
      true
    )
    .groupBy(['pp.property_id', 'properties.id', 'pp.url_path'])
    .orderBy(sorting ? [{
      column: sorting.split(" ")[0],
      order: sorting.split(" ")[1]
    }] : "properties.id");

  const totalCount = await properties.clone().count();

  const data = await properties.clone().modify(db =>
    maxResultCount != null ?
    db.limit(maxResultCount) :
    true
  ).modify(db =>
    skipCount != null ?
    db.offset(skipCount) :
    true
  ).select();

  return res.status(200).json({
    success: true,
    data: data,
    totalCount: totalCount.length
  });
});

/**
 * @swagger
 * path:
 *  /api/v1/property/GetPropertyForView:
 *    get:
 *      summary: Get a single property
 *      tags: [Properties]
 *      produces:
 *          - application/json
 *      parameters:
 *        - name: id
 *          in: query
 *          description: Property id
 *          required: true
 *          type: integer
 *      responses:
 *        "200":
 *          description: Returns all properties on the system
 */

exports.getPropertyForView = asyncHandler(async (req, res, next) => {
  db
    .select()
    .table("properties")
    .where("id", "=", req.query.id)
    .then(result => {
      return res.status(200).json(result[0]);
    });
});

/**
 * @swagger
 * path:
 *  /api/v1/property/CreateOrEdit:
 *    post:
 *      summary: Insert or edit a property
 *      tags: [Properties]
 *      produces:
 *          - application/json
 *      parameters:
 *        - name: Property
 *          in: body
 *          description: Property object
 *          required: true
 *          type: object
 *      responses:
 *        "200":
 *          description: Create or edit property
 */

exports.createOrEdit = asyncHandler(async (req, res, next) => {
  if (req.body.id == null) {
    this.insertProperty(req.body).then(() => {
      return res.status(200).json({
        success: true
      });
    }).catch(err => {
      return res.status(500).json({
        success: false,
        error: err.toString()
      });
    });
  } else {
    updateProperty(req, res, next);
  }
});

exports.insertProperty = async (property) => {
  return new Promise((resolve, reject) => {
    db("properties")
      .insert(property)
      .returning('id')
      .then((result) => {
        return resolve(result[0]);
      }).catch(err => {
        return reject(err);
      });
  });
};

const updateProperty = asyncHandler(async (req, res, next) => {
  db("properties")
    .where("id", "=", req.body.id)
    .update(req.body)
    .then(() => {
      return res.status(200).json({
        success: true
      });
    })
    .catch(err => {
      return res.status(500).json({
        success: false,
        error: err.toString()
      });
    });
});

/**
 * @swagger
 * path:
 *  /api/v1/property/Delete:
 *    delete:
 *      summary: Delete a property with a given id
 *      tags: [Properties]
 *      produces:
 *          - application/json
 *      parameters:
 *        - name: id
 *          in: query
 *          description: Property id
 *          required: true
 *          type: integer
 *      responses:
 *        "200":
 *          description: Success
 */

exports.deleteProperty = asyncHandler(async (req, res, next) => {
  db("property_pictures")
    .select()
    .where("property_id", "=", req.query.id)
    .then(result => {
      for (let item of result) {
        const public_id = path.parse(item.filename).name;
        cloudinary.uploader.destroy(public_id, {}, () => {});
        db("property_pictures").where("id", "=", item.id).del().then(() => {});
      }
      db("properties")
        .where("id", "=", req.query.id)
        .del()
        .then(() => {
          return res.status(200).json({
            success: true
          });
        })
        .catch(err => {
          return res.status(500).json({
            success: false,
            error: err.toString()
          });
        });
    });
});

/**
 * @swagger
 * path:
 *  /api/v1/property/ImportProperties:
 *    post:
 *      summary: Import properties from olx.com
 *      tags: [Properties]
 *      produces:
 *          - application/json
 *      responses:
 *        "200":
 *          description: Success
 */

exports.getRequest = async (url) => {
  return new Promise((resolve, reject) => {
    request.get({
        url: url,
        headers: {
          'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0'
        },
      },
      (err, httpResponse, body) => {
        if (err) {
          return reject(err.message);
        } else {
          return resolve(JSON.parse(httpResponse.body));
        }
      }
    );
  });
};

/*

id: number;
  title: string;
  description: string;
  bathrooms: number;
  bedrooms: number;
  garage: number;
  land_area: number;
  floor_area: number;
  price: number;
  address: string;
  city: string;
  postcode: number;
  province: string;

*/

exports.importProperties = asyncHandler(async (req, res, next) => {

  const propertyTitles = await db.select('title').table("properties").then();
  const result = await this.getRequest(process.env.PROPERTY_IMPORT_URL);
  const inputs = result.data;
  const properties = [];
  for (let data of inputs) {
    // Check if it has already existed
    const isExist = propertyTitles.find(item => item.title === data.title);
    if (!isExist) {
      const landArea = data.parameters.find(p => p.key === "p_sqr_land");
      const floorArea = data.parameters.find(p => p.key === "p_sqr_building");
      const bedrooms = data.parameters.find(p => p.key === "p_bedroom");
      const bathrooms = data.parameters.find(p => p.key === "p_bathroom");
      const address = data.parameters.find(p => p.key === "p_alamat");
      const city = data.location_source ? JSON.parse(data.location_source).name : null;
      const images = data.images;
      const property = {
        title: data.title,
        description: data.description,
        bathrooms: bathrooms ? Number(bathrooms.value) : 1,
        bedrooms: bedrooms ? Number(bedrooms.value) : 1,
        garage: null,
        land_area: landArea ? Number(landArea.value) : 0,
        floor_area: floorArea ? Number(floorArea.value) : 0,
        price: data.price.value.raw ? Number(data.price.value.raw) : 0,
        address: address ? address.value : null,
        city: city,
        postcode: null,
        province: city,
      }

      const propertyId = await this.insertProperty(property);

      for (let image of images) {
        const propertyPicture = {
          url_path: image.full.url,
          filename: image.external_id,
          uploaded_on: new Date(),
          property_id: propertyId
        };
        await db("property_pictures").insert(propertyPicture).then();
      }
      properties.push(property);
    }
    
  }
  return res.status(200).json({
    success: true,
    data: properties,
    totalCount: properties.length
  });
});