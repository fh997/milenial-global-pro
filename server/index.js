const express = require("express");
const bodyParser = require("body-parser");
const serveIndex = require("serve-index");
const cors = require("cors");
const dotenv = require("dotenv");
const morgan = require("morgan");
const swaggerUi = require("swagger-ui-express");
const fileupload = require('express-fileupload');
const specs = require("./swagger");
const path = require('path');

// Load env vars
dotenv.config({
  path: "./config/config.env"
});

// Route files
const properties = require("./routes/properties");
const employees = require("./routes/employees");
const propertyPictures = require("./routes/property_pictures");
// const users = require("./routes/users");
const auth = require("./routes/auth");
const app = express();

// Dev logging middleware
if (process.env.NODE_ENV === "development") {
  app.use(morgan("dev"));
}

// Body parser
app.use(bodyParser.json());

// Enable CORS
app.use(cors());

// File uploading
app.use(fileupload({
  createParentPath: true,
  useTempFiles: true,
  debug: true
}));

// Pagination config
app.all((req, res, next) => {
  // set default or minimum is 10 (as it was prior to v0.2.0)
  if (req.query.limit <= 10) req.query.limit = 10;
  next();
});

// Swagger API Documentation
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(specs));

// Mount routers
app.use("/api/v1/property", properties);
app.use("/api/v1/employee", employees);
app.use("/api/v1/property-picture", propertyPictures);
// app.use("/api/v1/users", users);
app.use("/api/v1/auth", auth);

// FTP configuration
app.use('/public', express.static('public'), serveIndex('public', {
  'icons': true
}));

// Redirect base url to swagger
app.get('/', (req, res) => res.redirect('/api-docs'));

const PORT = process.env.PORT || 5000;

const server = app.listen(
  PORT,
  console.log(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`)
);