import React, {useState} from "react";
import {
  AppBar,
  IconButton,
  Toolbar,
  Typography,
  Drawer,
  Divider,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  CssBaseline,
} from "@material-ui/core/";
import {useTheme} from "@material-ui/core/styles";
import {useHistory} from "react-router-dom";
import MenuIcon from "@material-ui/icons/Menu";
import HomeIcon from "@material-ui/icons/Home";
import PeopleIcon from "@material-ui/icons/People";
import PersonIcon from "@material-ui/icons/Person";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import logo from "../../../assets/images/logo.png";
import "./NavigationBar.scss";
import authenticationService from "../../../services/authentication/authenticationService";
import ImportExportIcon from '@material-ui/icons/ImportExport';

function NavigationBar(props: any) {
  const {container} = props;
  const theme = useTheme();
  const history = useHistory();

  /* React state hooks */
  const [mobileOpen, setMobileOpen] = useState(false);

  /* Drawer Component */
  const drawer = (
    <div className="w5">
      <img src={logo} alt="Milenial Global Pro" className="drawer-logo" />
      <Divider />
      <List>
        <ListItem button onClick={() => history.push("/admin/manage-listings")}>
          <ListItemIcon>
            <HomeIcon />
          </ListItemIcon>
          <ListItemText primary={"Manage Listings"} />
        </ListItem>
        <ListItem button onClick={() => history.push("/admin/import-listings")}>
          <ListItemIcon>
            <ImportExportIcon />
          </ListItemIcon>
          <ListItemText primary={"Import Listings"} />
        </ListItem>
        <div>
          <ListItem
            button
            onClick={() => history.push("/admin/manage-employees")}
          >
            <ListItemIcon>
              <PeopleIcon />
            </ListItemIcon>
            <ListItemText primary={"Manage Employees"} />
          </ListItem>
          {/* <ListItem button onClick={() => history.push("/admin/manage-users")}>
            <ListItemIcon>
              <AccountCircleIcon />
            </ListItemIcon>
            <ListItemText primary={"Manage Users"} />
          </ListItem> */}
        </div>
        {/* <Divider />
        <ListItem button onClick={() => history.push("/admin/edit-details")}>
          <ListItemIcon>
            <PersonIcon />
          </ListItemIcon>
          <ListItemText primary={"Edit Personal Details"} />
        </ListItem> */}
        <ListItem
          button
          onClick={() => {
            authenticationService.logout();
            history.push("/admin");
          }}
        >
          <ListItemIcon>
            <ExitToAppIcon />
          </ListItemIcon>
          <ListItemText primary={"Logout"} />
        </ListItem>
      </List>
    </div>
  );

  /* Handling open/close drawer */
  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  return (
    <div className="admin-navigation-bar">
      <CssBaseline />
      <AppBar position="fixed">
        <Toolbar>
          <IconButton
            color="inherit"
            edge="start"
            onClick={handleDrawerToggle}
            className="menu-button"
          >
            <MenuIcon />
          </IconButton>
          <div className="flex-auto-l">
            <Typography variant="h6" noWrap>
              {props.title}
            </Typography>
          </div>
          <div>{props.actionComponents}</div>
        </Toolbar>
        {props.tabBar}
      </AppBar>
      <Toolbar />
      {props.tabBar}
      <nav>
        <Drawer
          container={container}
          variant="temporary"
          anchor={theme.direction === "rtl" ? "right" : "left"}
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true,
          }}
        >
          {drawer}
        </Drawer>
      </nav>
    </div>
  );
}

export default NavigationBar;
