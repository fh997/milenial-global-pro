import http from "../httpService";
import {LoginInput} from "./dto/loginInput";

class AuthenticationService {
  public async login(input: LoginInput) {
    try {
      const result = await http.post("auth/Login", input);
      const token = result.data.token;
      const config = {
        headers: {
          Authorization: `Bearer ${token}`
        }
      };
      const getMe = await http.get("auth/Me", config);
      const currentUser = {
        ...getMe.data.data,
        token: token
      };
      localStorage.setItem("currentUser", JSON.stringify(currentUser));
      return currentUser;
    } catch (e) {
      return null;
    }
  }

  public getCurrentUser() {
    const user = localStorage.getItem("currentUser");
    if (user) return JSON.parse(user);
    return null;
  }

  public authHeader() {
    const currentUser = this.getCurrentUser();
    if (currentUser) {
      return {Authorization: `Bearer ${currentUser.token}`};
    } else {
      return {};
    }
  }

  public async logout() {
    localStorage.removeItem("currentUser");
  }
}

export default new AuthenticationService();
