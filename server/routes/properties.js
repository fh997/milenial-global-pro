const express = require("express");
const {
  protect
} = require('../middleware/auth');
const {
  getAll,
  getPropertyForView,
  createOrEdit,
  deleteProperty,
  importProperties
} = require("../controllers/properties");

const router = express.Router();

router.route("/GetAll").get(getAll);

router.route("/GetPropertyForView").get(getPropertyForView);

router.route("/CreateOrEdit").post(protect, createOrEdit);

router.route("/Delete").delete(protect, deleteProperty);

router.route("/ImportProperties").post(protect, importProperties);

module.exports = router;