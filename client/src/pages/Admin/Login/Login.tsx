import React, {Component} from "react";
import {RouteComponentProps} from "react-router";
import Logo from "../../../assets/images/logo.png";
import {withSnackbar, WithSnackbarProps} from "notistack";
import authenticationService from "../../../services/authentication/authenticationService";
import {Backdrop, CircularProgress} from "@material-ui/core";

interface ILoginProps extends RouteComponentProps<any>, WithSnackbarProps {}

interface ILoginStates {
  user: {
    username: string;
    password: string;
  };
  openBackdrop: boolean;
}

class Login extends Component<ILoginProps, ILoginStates> {
  constructor(props: ILoginProps) {
    super(props);
    this.state = {
      user: {
        username: "",
        password: "",
      },
      openBackdrop: false,
    };
  }

  public componentDidMount() {
    if (authenticationService.getCurrentUser()) {
      this.props.history.push("admin/manage-listings");
    }
  }

  public handleSubmit = async (event: any) => {
    event.preventDefault();
    this.setState({openBackdrop: true});
    const user = await authenticationService.login(this.state.user);
    this.setState({openBackdrop: false});
    if (!user)
      this.props.enqueueSnackbar("Invalid username or password", {
        variant: "error",
      });
    else this.props.history.push("admin/manage-listings");
  };

  public updateInputValue = (event: any) => {
    this.setState({
      user: {...this.state.user, [event.target.id]: event.target.value},
    });
  };

  render() {
    const {user, openBackdrop} = this.state;
    return (
      <div className="flex justify-center items-center vh-100">
        <div className="mw6 ma3">
          <img
            className="mb3"
            width="200"
            src={Logo}
            alt="Milenial Global Pro"
          />
          <h2 className="mb3">Login</h2>
          <form onSubmit={this.handleSubmit}>
            <p className="mb2">Username:</p>
            <input
              className="w-100 mb2"
              id="username"
              type="text"
              placeholder="Username"
              value={user.username}
              onChange={this.updateInputValue}
              required
            />
            <p className="mb2">Password:</p>
            <input
              className="w-100 mb3"
              id="password"
              type="password"
              placeholder="Password"
              value={user.password}
              onChange={this.updateInputValue}
              required
            />
            <button className="primaryButton mb3" type="submit">
              Login
            </button>
          </form>
          <p>Copyright © 2019 Milenial Global Pro.</p>
        </div>
        <Backdrop open={openBackdrop} style={{zIndex: 10}}>
          <CircularProgress color="primary" />
        </Backdrop>
      </div>
    );
  }
}

export default withSnackbar(Login);
